﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Domain
{
    /// <summary>
    /// Represents featured product records
    /// </summary>
    public partial class BS_FeaturedProducts : BaseEntity
    {
        public int ProductId { get; set; }
    }
}
