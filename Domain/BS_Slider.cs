﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Domain
{
    /// <summary>
    /// Represents featured product records
    /// </summary>
    public partial class BS_Slider : BaseEntity
    {
        public int PictureId { get; set; }
        public string OverrideAltAttribute { get; set; }
        public string OverrideTitleAttribute { get; set; }
        public string ShortDescription { get; set; }

        public DateTime? SliderActiveStartDate { get; set; }
        public DateTime? SliderActiveEndDate { get; set; }

        //public bool ForSlider { get; set; }
        //public bool ForBanner { get; set; }
        public int? CampaignType { get; set; }
        public int ImageFor { get; set; }

        public bool IsProduct { get; set; }
        public int ProdOrCatId { get; set; }

    }
}
