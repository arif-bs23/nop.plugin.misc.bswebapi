﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Misc.BsWebApi.Domain
{
    public partial class BS_ProductPriceWatch :BaseEntity
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public bool IsPriceWatch { get; set; }
    }
}
