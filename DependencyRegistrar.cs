﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Misc.BsWebApi.Data;
using Nop.Plugin.Misc.BsWebApi.Domain;
using Nop.Plugin.Misc.BsWebApi.Services;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<BsNopMobilePluginService>().As<IBsNopMobilePluginService>().InstancePerLifetimeScope();
            builder.RegisterType<ContentManagementService>().As<IContentManagementService>().InstancePerLifetimeScope();
            builder.RegisterType<ContentManagementTemplateService>().As<IContentManagementTemplateService>().InstancePerLifetimeScope();
            builder.RegisterType<CategoryIconService>().As<ICategoryIconService>().InstancePerLifetimeScope();
            builder.RegisterType<CategoryServiceApi>().As<ICategoryServiceApi>().InstancePerLifetimeScope();
            builder.RegisterType<BS_SliderService>().As<IBS_SliderService>().InstancePerLifetimeScope();
            builder.RegisterType<ProductPriceWatchServiceServiceApi>().As<IProductPriceWatchServiceApi>().InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<BswebApiObjectContext >(builder, "nop_object_context_BswebApi_plugin");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<BS_FeaturedProducts>>()
                .As<IRepository<BS_FeaturedProducts>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_BswebApi_plugin"))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BS_ContentManagement>>()
             .As<IRepository<BS_ContentManagement>>()
             .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_BswebApi_plugin"))
             .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BS_ContentManagementTemplate>>()
             .As<IRepository<BS_ContentManagementTemplate>>()
             .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_BswebApi_plugin"))
             .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BS_CategoryIcons>>()
             .As<IRepository<BS_CategoryIcons>>()
             .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_BswebApi_plugin"))
             .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BS_Slider>>()
        .As<IRepository<BS_Slider>>()
        .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_BswebApi_plugin"))
        .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<BS_ProductPriceWatch>>()
    .As<IRepository<BS_ProductPriceWatch>>()
    .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_BswebApi_plugin"))
    .InstancePerLifetimeScope();
        }

        public int Order
        {
            get { return 1; }
        }
    }
}
