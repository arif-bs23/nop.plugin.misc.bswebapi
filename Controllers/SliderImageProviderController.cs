﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Banner;
using Nop.Plugin.Misc.BsWebApi.PluginSettings;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Plugin.Misc.BsWebApi.Models.SliderImageModel;
using Nop.Plugin.Misc.BsWebApi.Services;
using Nop.Plugin.Misc.BsWebApi.Domain;

namespace Nop.Plugin.Misc.BsWebApi.Controllers
{
    public class SliderImageProviderController : BaseApiController
    {

        #region Field
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IPictureService _pictureService;
        private readonly IBS_SliderService _bsSliderService;

        #endregion

        #region Ctor
        public SliderImageProviderController(IPluginFinder pluginFinder,
            IStoreService storeService,
            IWorkContext workContext,
            IBS_SliderService bsSliderService,
            ISettingService settingService,
            IPictureService pictureService)
        {
            this._pluginFinder = pluginFinder;
            this._storeService = storeService;
            this._workContext = workContext;
            this._settingService = settingService;
            this._pictureService = pictureService;
            this._bsSliderService = bsSliderService;

        }
        #endregion

        #region Utility

        //private HomePageBannerResponseModel.BannerModel GetPictureUrl(int pictureId)
        //{
        //    var imageUrl = _pictureService.GetPictureUrl(pictureId, showDefaultPicture: false);
        //    var picture = new HomePageBannerResponseModel.BannerModel()
        //    {
        //        ImageUrl = imageUrl == string.Empty ? null : imageUrl
        //    };
        //    return picture;
        //}
        #endregion

        #region Action Method
        //[Route("api/homepagebanner")]
        //[HttpGet]
        //public IHttpActionResult HomePageBanner()
        //{
        //    PluginDescriptor nivoSliderPlugin = _pluginFinder.GetPluginDescriptorBySystemName("Widgets.NivoSlider");
        //    var isValid = nivoSliderPlugin != null && nivoSliderPlugin.Installed;
        //    var result = new HomePageBannerResponseModel();
        //    if (isValid)
        //    {
        //        var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
        //        var nivoSliderSettings = _settingService.LoadSetting<BsNopMobileSettings>(storeScope);
        //        var pictureList = new List<HomePageBannerResponseModel.BannerModel>
        //        {
        //            new HomePageBannerResponseModel.BannerModel()
        //            {
        //                ImageUrl = GetPictureUrl(nivoSliderSettings.Picture1Id).ImageUrl,
        //                Text = nivoSliderSettings.Text1,
        //                Link = nivoSliderSettings.Link1,
        //                IsProduct = nivoSliderSettings.IsProduct1 == true ? 1 : 0,
        //                ProdOrCatId = nivoSliderSettings.ProductOrCategoryId1

        //            },
        //            new HomePageBannerResponseModel.BannerModel()
        //            {
        //                ImageUrl = GetPictureUrl(nivoSliderSettings.Picture2Id).ImageUrl,
        //                Text = nivoSliderSettings.Text2,
        //                Link = nivoSliderSettings.Link2,
        //                IsProduct = nivoSliderSettings.IsProduct2 == true ? 1 : 0,
        //                ProdOrCatId = nivoSliderSettings.ProductOrCategoryId2

        //            },
        //            new HomePageBannerResponseModel.BannerModel()
        //            {
        //                ImageUrl = GetPictureUrl(nivoSliderSettings.Picture3Id).ImageUrl,
        //                Text = nivoSliderSettings.Text3,
        //                Link = nivoSliderSettings.Link3,
        //                IsProduct = nivoSliderSettings.IsProduct3 == true ? 1 : 0,
        //                ProdOrCatId = nivoSliderSettings.ProductOrCategoryId3

        //            },
        //            new HomePageBannerResponseModel.BannerModel()
        //            {
        //                ImageUrl = GetPictureUrl(nivoSliderSettings.Picture4Id).ImageUrl,
        //                Text = nivoSliderSettings.Text4,
        //                Link = nivoSliderSettings.Link4,
        //                IsProduct = nivoSliderSettings.IsProduct4 == true ? 1 : 0,
        //                ProdOrCatId = nivoSliderSettings.ProductOrCategoryId4

        //            },
        //            new HomePageBannerResponseModel.BannerModel()
        //            {
        //                ImageUrl = GetPictureUrl(nivoSliderSettings.Picture5Id).ImageUrl,
        //                Text = nivoSliderSettings.Text5,
        //                Link = nivoSliderSettings.Link5,
        //                IsProduct = nivoSliderSettings.IsProduct5 == true ? 1 : 0,
        //                ProdOrCatId = nivoSliderSettings.ProductOrCategoryId5

        //            }
        //            //GetPictureUrl(nivoSliderSettings.Picture1Id),
        //            //GetPictureUrl(nivoSliderSettings.Picture2Id),
        //            //GetPictureUrl(nivoSliderSettings.Picture3Id),
        //            //GetPictureUrl(nivoSliderSettings.Picture4Id),
        //            //GetPictureUrl(nivoSliderSettings.Picture5Id)
        //        };
        //        result.IsEnabled = true;
        //        result.Data = pictureList;
        //    }
        //    else
        //    {
        //        result.IsEnabled = false;
        //    }
        //    return Ok(result);
        //}

        [Route("api/getsliderimage/{todayDate}")]
        [HttpGet]
        public IHttpActionResult SliderImage(DateTime TodayDate)
        {

            List<SliderBannerModel> SliderBannerModelList = new List<SliderBannerModel>();

            var sliderDomainList = _bsSliderService.GetBSSliderImagesByDate(TodayDate);

            foreach (var sliderDomain in sliderDomainList)
            {
                var picture = _pictureService.GetPictureById(sliderDomain.PictureId);
                SliderBannerModelList.Add(new SliderBannerModel
                {
                    ImageUrl = _pictureService.GetPictureUrl(picture),
                    SuccessMessage="Today's slider image",
                    IsSlider = sliderDomain.ForSlider,
                    IsBanner=sliderDomain.ForBanner
                });
            }


            return Ok(SliderBannerModelList);
        }
        #endregion

    }
}
