﻿using System;
using System.Web.Http;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Stores;

namespace Nop.Plugin.Misc.BsWebApi.Controllers
{
    public class TopicsController : BaseApiController
    {
        #region Field
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        #endregion

        #region Constructor

        public TopicsController(IWorkContext workContext, IStoreService storeService, ISettingService settingService, ILocalizationService localizationService)
        {
            _workContext = workContext;
            _storeService = storeService;
            _settingService = settingService;
            _localizationService = localizationService;
        }

        #endregion

        #region Action Method
        [HttpGet]
        [Route("api/contactinfo")]
        public IHttpActionResult ContactInfo()
        {
            var languageStringForAvailableCallTime = "plugins.nopmobile.webapi.availablecalltime";

            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var bsNopMobileSettings = _settingService.LoadSetting<BsNopMobileSettings>(storeScope);

            string number = bsNopMobileSettings.MobContactNumber;
            string email = bsNopMobileSettings.MobContactEmail;
            string callTime = _localizationService.GetResource(languageStringForAvailableCallTime);

            var data = new
            {
                ContactNumber = number,
                ContactEmail = email,
                ContactTime = (callTime == languageStringForAvailableCallTime)?string.Empty: callTime
            };

            return Ok(data);
        }
        #endregion
    }
}
