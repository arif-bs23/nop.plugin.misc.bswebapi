﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Misc.BsWebApi.Extensions;
using Nop.Plugin.Misc.BsWebApi.Infrastructure.Cache;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models._Common;
using Nop.Plugin.Misc.BsWebApi.Models._ResponseModel;
using Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Catalog;
using Nop.Plugin.Misc.BsWebApi.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework.Events;
using System.Globalization;
using Nop.Core.Domain.Localization;
using Nop.Web.Models.Media;
using Nop.Plugin.Misc.BsWebApi.Models.Vendor;
using Nop.Plugin.Misc.BsWebApi.Models.Product;

namespace Nop.Plugin.Misc.BsWebApi.Controllers
{
    public class CatalogController : BaseApiController
    {
        #region Fields

        private int pageSize = 25;

        private readonly ICategoryService _categoryService;
        private readonly ICategoryServiceApi _categoryServiceapi;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPermissionService _permissionService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly ICurrencyService _currencyService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductService _productService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly IProductServiceApi _productServiceApi;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ISearchTermService _searchTermService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProductTagService _productTagService;
        private readonly IVendorService _vendorService;
        private readonly VendorSettings _vendorSettings;
        private readonly ICategoryIconService _categoryIconService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly ILanguageService _languageService;
        private readonly ICustomerServiceApi _customerServiceApi;
        private readonly IOrderReportService _orderReportService;
        #endregion

        #region Ctor
        public CatalogController(ICategoryService categoryService,
            ICategoryServiceApi categoryServiceapi,
            IWorkContext workContext,
            IStoreContext storeContext,
            IWebHelper webHelper,
            ICacheManager cacheManager,
            IPictureService pictureService,
            MediaSettings mediaSettings,
            IManufacturerService manufacturerService,
            IPermissionService permissionService,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            ICurrencyService currencyService,
            CatalogSettings catalogSettings,
            IPriceFormatter priceFormatter,
            IProductService productService,
            ISpecificationAttributeService specificationAttributeService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            IProductServiceApi productServiceApi,
            IGenericAttributeService genericAttributeService,
            ISearchTermService searchTermService,
            IEventPublisher eventPublisher,
            IProductTagService productTagService,
            IVendorService vendorService,
            VendorSettings vendorSettings,
            ICategoryIconService categoryIconService,
            LocalizationSettings localizationSettings,
            ILanguageService languageService,
            ICustomerServiceApi customerServiceApi,
            IOrderReportService orderReportService
            )
        {
            this._categoryService = categoryService;
            this._categoryServiceapi = categoryServiceapi;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._webHelper = webHelper;
            this._cacheManager = cacheManager;
            this._pictureService = pictureService;
            this._mediaSettings = mediaSettings;
            this._manufacturerService = manufacturerService;
            this._permissionService = permissionService;
            this._aclService = aclService;
            this._storeMappingService = storeMappingService;
            this._currencyService = currencyService;
            this._catalogSettings = catalogSettings;
            this._priceFormatter = priceFormatter;
            this._productService = productService;
            this._specificationAttributeService = specificationAttributeService;
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._priceCalculationService = priceCalculationService;
            this._taxService = taxService;
            this._productServiceApi = productServiceApi;
            this._genericAttributeService = genericAttributeService;
            this._searchTermService = searchTermService;
            this._eventPublisher = eventPublisher;
            this._productTagService = productTagService;
            this._vendorService = vendorService;
            this._vendorSettings = vendorSettings;
            this._categoryIconService = categoryIconService;
            this._localizationSettings = localizationSettings;
            this._languageService = languageService;
            this._customerServiceApi = customerServiceApi;
            this._orderReportService = orderReportService;
        }
        #endregion

        #region Utility

        [System.Web.Http.NonAction]
        protected virtual void PrepareViewModes(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            if (command == null)
                throw new ArgumentNullException("command");

            pagingFilteringModel.AllowProductViewModeChanging = _catalogSettings.AllowProductViewModeChanging;

            var viewMode = !string.IsNullOrEmpty(command.ViewMode)
                ? command.ViewMode
                : _catalogSettings.DefaultViewMode;
            pagingFilteringModel.ViewMode = viewMode;
            if (pagingFilteringModel.AllowProductViewModeChanging)
            {
                var currentPageUrl = _webHelper.GetThisPageUrl(true);
                //grid
                pagingFilteringModel.AvailableViewModes.Add(new SelectListItem
                {
                    Text = _localizationService.GetResource("Catalog.ViewMode.Grid"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=grid", null),
                    Selected = viewMode == "grid"
                });
                //list
                pagingFilteringModel.AvailableViewModes.Add(new SelectListItem
                {
                    Text = _localizationService.GetResource("Catalog.ViewMode.List"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=list", null),
                    Selected = viewMode == "list"
                });
            }

        }

        [System.Web.Http.NonAction]
        protected virtual void PreparePageSizeOptions(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command,
            bool allowCustomersToSelectPageSize, string pageSizeOptions, int fixedPageSize)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            if (command == null)
                throw new ArgumentNullException("command");

            if (command.PageNumber <= 0)
            {
                command.PageNumber = 1;
            }
            pagingFilteringModel.AllowCustomersToSelectPageSize = false;
            if (allowCustomersToSelectPageSize && pageSizeOptions != null)
            {
                var pageSizes = pageSizeOptions.Split(new [] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (pageSizes.Any())
                {
                    // get the first page size entry to use as the default (category page load) or if customer enters invalid value via query string
                    if (command.PageSize <= 0 || !pageSizes.Contains(command.PageSize.ToString()))
                    {
                        int temp;
                        if (int.TryParse(pageSizes.FirstOrDefault(), out temp))
                        {
                            if (temp > 0)
                            {
                                command.PageSize = temp;
                            }
                        }
                    }

                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "pagesize={0}", null);
                    sortUrl = _webHelper.RemoveQueryString(sortUrl, "pagenumber");

                    foreach (var pageSize in pageSizes)
                    {
                        int temp;
                        if (!int.TryParse(pageSize, out temp))
                        {
                            continue;
                        }
                        if (temp <= 0)
                        {
                            continue;
                        }

                        pagingFilteringModel.PageSizeOptions.Add(new SelectListItem
                        {
                            Text = pageSize,
                            Value = String.Format(sortUrl, pageSize),
                            Selected = pageSize.Equals(command.PageSize.ToString(), StringComparison.InvariantCultureIgnoreCase)
                        });
                    }

                    if (pagingFilteringModel.PageSizeOptions.Any())
                    {
                        pagingFilteringModel.PageSizeOptions = pagingFilteringModel.PageSizeOptions.OrderBy(x => int.Parse(x.Text)).ToList();
                        pagingFilteringModel.AllowCustomersToSelectPageSize = true;

                        if (command.PageSize <= 0)
                        {
                            command.PageSize = int.Parse(pagingFilteringModel.PageSizeOptions.FirstOrDefault().Text);
                        }
                    }
                }
            }
            else
            {
                //customer is not allowed to select a page size
                command.PageSize = fixedPageSize;
            }

            //ensure pge size is specified
            if (command.PageSize <= 0)
            {
                command.PageSize = fixedPageSize;
            }
        }

        [System.Web.Http.NonAction]
        protected virtual IEnumerable<ProductOverViewModelApi> PrepareProductOverviewModels(IEnumerable<Product> products,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false, bool isAssociatedProduct = false)
        {
            return products.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, _specificationAttributeService,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes, isAssociatedProduct);
        }

        [System.Web.Http.NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_CHILD_IDENTIFIERS_MODEL_KEY,
                parentCategoryId,
                string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                _storeContext.CurrentStore.Id);
            return _cacheManager.Get(cacheKey, () =>
            {
                var categoriesIds = new List<int>();
                var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId);
                foreach (var category in categories)
                {
                    categoriesIds.Add(category.Id);
                    categoriesIds.AddRange(GetChildCategoryIds(category.Id));
                }
                return categoriesIds;
            });
        }


        [System.Web.Http.NonAction]
        protected virtual void PrepareSortingOptions(CatalogPagingFilteringModel pagingFilteringModel, int orderBy)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            pagingFilteringModel.AllowProductSorting = _catalogSettings.AllowProductSorting;
            if (pagingFilteringModel.AllowProductSorting)
            {
                foreach (SortByList enumValue in Enum.GetValues(typeof(SortByList)))
                {
                    
                    var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                    pagingFilteringModel.AvailableSortOptions.Add(new SelectListItem
                    {
                        Text = sortValue,
                        Value = string.Concat((int)enumValue),
                        Selected = enumValue == (SortByList)orderBy
                    });
                }
            }
            
        }

        [System.Web.Http.NonAction]
        protected virtual void PrepareSearchingOptions(CatalogPagingFilteringModel pagingFilteringModel, int searchBy)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");


            foreach (SearchByList enumValue in Enum.GetValues(typeof(SearchByList)))
            {
               
                var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                pagingFilteringModel.AvailableSearchOptions.Add(new SelectListItem
                {
                    Text = sortValue,
                    Value = string.Concat((int)enumValue),
                    Selected = enumValue == (SearchByList)searchBy
                });
            }
        }

        [System.Web.Http.NonAction]
        protected virtual void PrepareFilteringOptions(CatalogPagingFilteringModel pagingFilteringModel, int filterBy)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            foreach (FilterByList enumValue in Enum.GetValues(typeof(FilterByList)))
            {
                var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                pagingFilteringModel.AvailableFilteringOptions.Add(new SelectListItem
                {
                    Text = sortValue,
                    Value = string.Concat((int)enumValue),
                    Selected = enumValue == (FilterByList)filterBy
                });
            }
            
        }

        protected IList<Product> GetProductsByCategoryId(int categoryId, int itemsNumber)
        {
            var categoryIds = new List<int> {categoryId};
            if (_catalogSettings.ShowProductsFromSubcategories)
            {
                //include subcategories
                categoryIds.AddRange(GetChildCategoryIds(categoryId));
            }
            //products
            var products= new List<Product>();
            var featuredProducts = _productService.SearchProducts(
                       categoryIds: new List<int> { categoryId },
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: true);
            products.AddRange(featuredProducts);
            int remainingProducts =  itemsNumber-products.Count();
            if (remainingProducts>0)
            {
                IList<int> filterableSpecificationAttributeOptionIds;
                var extraProducts = _productService.SearchProducts(out filterableSpecificationAttributeOptionIds, false,
                categoryIds: categoryIds,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                featuredProducts: false,
                orderBy: (ProductSortingEnum)15,
                pageSize: itemsNumber,
                pageIndex: 0);
                products.AddRange(extraProducts);
            }
            return products;
        }

        protected IList<Product> GetAllProductsByCategoryId(int categoryId)
        {
            var categoryIds = new List<int> { categoryId };
            if (_catalogSettings.ShowProductsFromSubcategories)
            {
                //include subcategories
                categoryIds.AddRange(GetChildCategoryIds(categoryId));
            }
            //products
            var products = _productService.SearchProducts(
                       categoryIds: categoryIds,
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true);
            
            return products;
        }

        [System.Web.Http.NonAction]
        protected virtual IEnumerable<SubCategoryModelApi> PrepareCategoryFilterOnSale(IEnumerable<Product> products, int pictureSize)
        {
            List<SubCategoryModelApi> categoryList = new List<SubCategoryModelApi>();

            foreach (var item in products)
	        {
                var cList = _categoryService.GetProductCategoriesByProductId(item.Id).ToList().Select(s => new SubCategoryModelApi() { 
                    Id = s.CategoryId,
                    Name = s.Category.Name,
                    PictureModel = new PictureModel()
                            {
                                FullSizeImageUrl = _pictureService.GetPictureUrl(s.Category.PictureId),
                                ImageUrl = _pictureService.GetPictureUrl(s.Category.PictureId, pictureSize),
                                Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), s.Category.Name),
                                AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), s.Category.Name)
                            }
                    
                });

                categoryList = categoryList.Union(cList).ToList();
	        }
            
            return categoryList;
        }


        protected virtual IEnumerable<SearchTermAutoCompleteApiModel> PrepareSearchTermAutoCompleteApiModel(
            IEnumerable<Category> categories)
        {
            List<SearchTermAutoCompleteApiModel> categoryList = new List<SearchTermAutoCompleteApiModel>();
            foreach (var category in categories)
            {
                var categoriesnew = new SearchTermAutoCompleteApiModel()
                {
                    Id = category.Id,
                    CategoryName = category.Name
                };
                categoryList.Add(categoriesnew);
            }
            return categoryList.ToList();

        }
        #endregion

        #region Action Method

        #region category
        [System.Web.Http.Route("api/categories")]
        [System.Web.Http.HttpGet]
        //[TokenAuthorize]
        public IHttpActionResult Categories()
        {
            var categories = _categoryService.GetAllCategories();
            string categoryCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_MENU_MODEL_KEY,
                _workContext.WorkingLanguage.Id,
                string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                _storeContext.CurrentStore.Id);
            var cachedCategoriesModel = _cacheManager.Get(categoryCacheKey, () => _categoryService.GetAllCategories());

            var model = cachedCategoriesModel.MapTo<Category, CategoryNavigationModelApi>();
            foreach (var cat in model)
            {
                string fileName = "ic_new_category.png";
                if (_categoryIconService.GetIconExtentionByCategoryId(cat.Id) != null)
                {
                    fileName = string.Format("{0}{1}", cat.Id.ToString(), _categoryIconService.GetIconExtentionByCategoryId(cat.Id).Extension);
                }

                cat.IconPath = fileName.IconImagePath(_webHelper);
            }
            int count = _workContext.CurrentCustomer.ShoppingCartItems
                       .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                       .LimitPerStore(_storeContext.CurrentStore.Id)
                       .ToList()
                       .GetTotalProducts();

            // Language Selector

            var availableLanguages = _cacheManager.Get(string.Format(ModelCacheEventConsumer.AVAILABLE_LANGUAGES_MODEL_KEY, _storeContext.CurrentStore.Id), () =>
            {
                var languages = _languageService
                    .GetAllLanguages(storeId: _storeContext.CurrentStore.Id)
                    .Select(x => new LanguageNavModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        FlagImageFileName = x.FlagImageFileName,
                    })
                    .ToList();
                return languages;
            });

            var langNavModel = new LanguageNavSelectorModel
            {
                CurrentLanguageId = _workContext.WorkingLanguage.Id,
                AvailableLanguages = availableLanguages,
                UseImages = _localizationSettings.UseImagesForLanguageSelection
            };
            
            // Currency Selector

            var availableCurrencies = _cacheManager.Get(string.Format(ModelCacheEventConsumer.AVAILABLE_CURRENCIES_MODEL_KEY, _workContext.WorkingLanguage.Id, _storeContext.CurrentStore.Id), () =>
            {
                var currencies = _currencyService
                    .GetAllCurrencies(storeId: _storeContext.CurrentStore.Id)
                    .Select(x =>
                    {
                        //currency char
                        var currencySymbol = "";
                        if (!string.IsNullOrEmpty(x.DisplayLocale))
                            currencySymbol = new RegionInfo(x.DisplayLocale).CurrencySymbol;
                        else
                            currencySymbol = x.CurrencyCode;
                        //model
                        var currencyModel = new CurrencyNavModel
                        {
                            Id = x.Id,
                            Name = x.GetLocalized(y => y.Name),
                            CurrencySymbol = currencySymbol
                        };
                        return currencyModel;
                    })
                    .ToList();
                return currencies;
            });

            var currNavModel = new CurrencyNavSelectorModel
            {
                CurrentCurrencyId = _workContext.WorkingCurrency.Id,
                AvailableCurrencies = availableCurrencies
            };


            var result = new AllResponseModel { Data = model, Count = count, Language = langNavModel, Currency = currNavModel };
            return Ok(result);

        }

        //[System.Web.Http.Route("api/subcategories")]
        //[System.Web.Http.HttpGet]
        ////[TokenAuthorize]
        //public IHttpActionResult SubCategories()
        //{
        //    //var categories = _categoryService.GetAllCategories();
        //    string categoryCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_MENU_MODEL_KEY,
        //        _workContext.WorkingLanguage.Id,
        //        string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
        //        _storeContext.CurrentStore.Id);
        //    var cachedCategoriesModel = _cacheManager.Get(categoryCacheKey, () => _categoryService.GetAllCategories());

        //    var parentCategories = cachedCategoriesModel.Where(w => w.ParentCategoryId == 0).ToList();

        //    var subCategories = cachedCategoriesModel.Join(parentCategories, p=>p.ParentCategoryId,q=>q.Id,(p,q)=>new {p}).ToList();

        //    var model = subCategories.Select(s => new CategoryNavigationModelApi() { 
        //        Id = s.p.Id,
        //        Name = s.p.Name,
        //        ParentCategoryId = s.p.ParentCategoryId,
        //        DisplayOrder = s.p.DisplayOrder,
        //        IconPath = string.Format("{0}{1}",s.p.Id.ToString(),_categoryIconService.GetIconExtentionByCategoryId(s.p.Id).Extension).IconImagePath(_webHelper)
        //    }).ToList();
        //    //foreach (var cat in model)
        //    //{
        //    //    string fileName = cat.Id + ".png";
        //    //    cat.IconPath = fileName.IconImagePath(_webHelper);
        //    //}
        //    int count = _workContext.CurrentCustomer.ShoppingCartItems
        //               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
        //               .LimitPerStore(_storeContext.CurrentStore.Id)
        //               .ToList()
        //               .GetTotalProducts();
        //    var result = new AllCategoriesResponseModel { Data = model, Count = count };
        //    return Ok(result);
        //}

        [System.Web.Http.Route("api/homepagecategories")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult HomepageCategories(int? thumbPictureSize = null)
        {
            string categoriesCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_HOMEPAGE_KEY,
               string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
               _storeContext.CurrentStore.Id,
               _workContext.WorkingLanguage.Id,
               _webHelper.IsCurrentConnectionSecured());

            var model = _cacheManager.Get(categoriesCacheKey, () =>
                _categoryService.GetAllCategoriesDisplayedOnHomePage()
                .Select(x =>
                {
                    var catModel = x.MapTo<Category, CategoryOverViewModelApi>();
                    int pictureSize = _mediaSettings.CategoryThumbPictureSize;
                    //prepare picture model
                    if (thumbPictureSize.HasValue)
                    {
                        pictureSize = thumbPictureSize.GetValueOrDefault();
                    }

                    var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, x.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                    catModel.DefaultPictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                    {
                        var picture = _pictureService.GetPictureById(x.PictureId);
                        var pictureModel = new PictureModel
                        {
                            ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        };
                        return pictureModel;
                    });
                    catModel.ProductCount = GetAllProductsByCategoryId(catModel.Id).Count;
                    return catModel;
                })
                .ToList()
            );

            var result = new GeneralResponseModel<List<CategoryOverViewModelApi>>()
            {
                Data = model
            };
            return Ok(result);
        }



        [System.Web.Http.Route("api/catalog/homepagecategorieswithproduct")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult HomepageCategoriesWithProduct(int? thumbPictureSize = null)
        {
            string categoriesCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_HOMEPAGE_KEY_PRODUCT_CATEGORY,
               string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
               _storeContext.CurrentStore.Id,
               _workContext.WorkingLanguage.Id,
               _webHelper.IsCurrentConnectionSecured());

            var model = _cacheManager.Get(categoriesCacheKey, () =>
                _categoryService.GetAllCategoriesDisplayedOnHomePage()
                .Select(x =>
                {
                    var catWithProduct = new CatalogFeaturedCategoryWithProduct();
                    var subCategory = _categoryService.GetAllCategoriesByParentCategoryId(x.Id).Select(c => new CatalogFeaturedCategoryWithProduct.SubCategoriesWithNameAndId() { Id = c.Id, Name = c.Name, IconPath = _categoryIconService.GetIconExtentionByCategoryId(c.Id) != null ? string.Format("{0}{1}", c.Id.ToString(), _categoryIconService.GetIconExtentionByCategoryId(c.Id)).IconImagePath(_webHelper) : ("defaultIcon.png").IconImagePath(_webHelper) }).ToList();
                    //var categoryIds = subCategory.Select(c => c.Id).ToList();
                    var catModel = x.MapTo<Category, CategoryOverViewModelApi>();
                    int pictureSize = _mediaSettings.CategoryThumbPictureSize;
                    //prepare picture model
                    if (thumbPictureSize.HasValue)
                    {
                        pictureSize = thumbPictureSize.GetValueOrDefault();
                    }

                    var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, x.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                    catModel.DefaultPictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                    {
                        var picture = _pictureService.GetPictureById(x.PictureId);
                        var pictureModel = new PictureModel
                        {
                            ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        };
                        return pictureModel;
                    });
                    catWithProduct.SubCategory = subCategory;                    
                    var products =  GetProductsByCategoryId(x.Id, 6);
                    catWithProduct.Product = PrepareProductOverviewModels(products);
                    catModel.ProductCount = products.Count;
                    catWithProduct.Category = catModel;
                    return catWithProduct;
                })
                .ToList()
            );

            var result = new GeneralResponseModel<List<CatalogFeaturedCategoryWithProduct>>()
            {
                Data = model
            };
            return Ok(result);
        }

        [System.Web.Http.Route("api/Category/{categoryId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult Category(int categoryId, int pageNumber = 1, int orderBy = 0, int searchBy = 0, int filterBy = 0, int pageSize = 25)
        {
            var category = _categoryService.GetCategoryById(categoryId);

            if (category == null || category.Deleted)
                return NotFound();

            //Check whether the current user has a "Manage catalog" permission
            //It allows him to preview a category before publishing
            if (!category.Published && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories))
                return NotFound();

            //ACL (access control list)
            if (!_aclService.Authorize(category))
                return NotFound();

            //Store mapping
            if (!_storeMappingService.Authorize(category))
                return NotFound();

            var model = category.ToModel();

            PrepareSortingOptions(model.PagingFilteringContext, orderBy);
            PrepareSearchingOptions(model.PagingFilteringContext, searchBy);
            PrepareFilteringOptions(model.PagingFilteringContext, filterBy);

            #region Retrieve product tag in a category 

            /*//var categories = _categoryService.GetAllCategoriesByParentCategoryId(categoryId);
            var productCategories = _categoryService.GetProductCategoriesByCategoryId(categoryId, 0, int.MaxValue, true);

            var tagsInCategory = new List<ProductTag>();

            foreach (var cat in productCategories)
            {

                var productTags = cat.Product.ProductTags;

                foreach (var tag in productTags)
                {
                    if (tag != null)
                    {
                        var newProductTag = new ProductTag
                        {
                            Id = tag.Id,
                            Name = tag.Name
                        };

                        if (!tagsInCategory.Contains(newProductTag))
                        {
                            tagsInCategory.Add(newProductTag);
                        }
                    }
                }
            }

            model.Tags = tagsInCategory;*/

            #endregion

            model.PagingFilteringContext.PriceRangeFilter.LoadPriceRangeFilters(category.PriceRanges, _webHelper, _priceFormatter);
            //var range = category.PriceRanges;
            var selectedPriceRange = model.PagingFilteringContext.PriceRangeFilter.GetSelectedPriceRange(_webHelper, category.PriceRanges);
            decimal? minPriceConverted = null;
            decimal? maxPriceConverted = null;
            if (selectedPriceRange != null)
            {
                if (selectedPriceRange.From.HasValue)
                    minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.From.Value, _workContext.WorkingCurrency);

                if (selectedPriceRange.To.HasValue)
                    maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.To.Value, _workContext.WorkingCurrency);
            }

            var categoryIds = new List<int>();
            categoryIds.Add(category.Id);
            if (_catalogSettings.ShowProductsFromSubcategories)
            {
                //include subcategories
                categoryIds.AddRange(GetChildCategoryIds(category.Id));
            }
            //products
            IList<int> alreadyFilteredSpecOptionIds = model.PagingFilteringContext.SpecificationFilter.GetAlreadyFilteredSpecOptionIds(_webHelper);

            IList<int> alreadyFilteredBrandOptionIds = model.PagingFilteringContext.BrandFilter.GetAlreadyFilteredBrandOptionIds(_webHelper);

            IList<int> alreadyFilteredTagOptionIds = model.PagingFilteringContext.TagFilter.GetAlreadyFilteredTagOptionIds(_webHelper);

            IList<int> filterableSpecificationAttributeOptionIds;

            //TODO: Need remove "productsInCategory" and should manage "products"
            var productsInCategory = _productServiceApi.SearchProductsOnApi(
                out filterableSpecificationAttributeOptionIds,true,
                categoryIds: categoryIds,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                hasDiscountApplied: false,
                hasSpecialPrice: false,
                hasTierPrice: false);

            var products = _productServiceApi.SearchProductsOnApi(
                    out filterableSpecificationAttributeOptionIds,
                    true,
                    categoryIds: categoryIds,
                    manufacturerIds: alreadyFilteredBrandOptionIds, //Brands
                    storeId: _storeContext.CurrentStore.Id,
                    visibleIndividuallyOnly: true,
                    priceMin: minPriceConverted,
                    priceMax: maxPriceConverted,
                    filteredSpecs: alreadyFilteredSpecOptionIds, //Speciality
                    orderBy: (SortByList)orderBy,
                    searchBy: (SearchByList)searchBy,
                    productTags: alreadyFilteredTagOptionIds, //Tags
                    pageSize: pageSize,
                    pageIndex: pageNumber - 1,
                    hasDiscountApplied: false,
                    hasSpecialPrice: false,
                    hasTierPrice: false);


            #region Retrieve product tag from products

            var tagsInCategory = new List<ProductTag>();

            foreach (var product in productsInCategory.ToList())
            {
                var productTags = product.ProductTags;

                foreach (var tag in productTags)
                {
                    if (tag != null)
                    {
                        var newProductTag = new ProductTag
                        {
                            Id = tag.Id,
                            Name = tag.Name
                        };

                        if (!tagsInCategory.Contains(newProductTag))
                        {
                            tagsInCategory.Add(newProductTag);
                        }
                    }
                }
            }

            model.Tags = tagsInCategory;

            #endregion

            var price = _productServiceApi.SearchProductsPrice(
                categoryIds: categoryIds,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                featuredProducts: _catalogSettings.IncludeFeaturedProductsInNormalLists ? null : (bool?)false,
                priceMin: minPriceConverted,
                priceMax: maxPriceConverted,
                filteredSpecs: alreadyFilteredSpecOptionIds,
                orderBy: (ProductSortingEnum)orderBy,
                pageSize: this.pageSize,
                pageIndex: pageNumber - 1);

            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.TotalPages = products.TotalPages;

           // model.PagingFilteringContext.LoadPagedList(products);

            //specs
            model.PagingFilteringContext.SpecificationFilter.PrepareSpecsFilters(alreadyFilteredSpecOptionIds,
                filterableSpecificationAttributeOptionIds,
                _specificationAttributeService, _webHelper, _workContext);

            model.PagingFilteringContext.BrandFilter.PrepareBrandFilters(alreadyFilteredBrandOptionIds, _manufacturerService, _productService,_webHelper, _workContext);
            //var Model=model.PagingFilteringContext.SpecificationFilter.NotFilteredItems;

            var result = model.ToModel(price: price);
            return Ok(result);
        }

        [System.Web.Http.Route("api/categoryfeaturedproductandsubcategory/{categoryId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult CategoryFeaturedProductAndSubCategory(int categoryId)
        {
            var category = _categoryService.GetCategoryById(categoryId);
            if (category == null || category.Deleted)
                return NotFound();

            //Check whether the current user has a "Manage catalog" permission
            //It allows him to preview a category before publishing
            if (!category.Published && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories))
                return NotFound();

            //ACL (access control list)
            if (!_aclService.Authorize(category))
                return NotFound();

            //Store mapping
            if (!_storeMappingService.Authorize(category))
                return NotFound();

            var model = category.ToModel();


            //subcategories
            string subCategoriesCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_SUBCATEGORIES_KEY,
                categoryId,
                string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                _storeContext.CurrentStore.Id,
                _workContext.WorkingLanguage.Id,
                _webHelper.IsCurrentConnectionSecured());
            model.SubCategories = _cacheManager.Get(subCategoriesCacheKey, () =>
                _categoryService.GetAllCategoriesByParentCategoryId(categoryId)
                .Select(x =>
                {
                    var subCatModel = new SubCategoryModelApi
                    {
                        Id = x.Id,
                        Name = x.GetLocalized(y => y.Name)
                    };

                    //prepare picture model
                    int pictureSize = _mediaSettings.CategoryThumbPictureSize;
                    var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, x.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                    subCatModel.PictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                    {
                        var picture = _pictureService.GetPictureById(x.PictureId);
                        var pictureModel = new PictureModel
                        {
                            ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize)
                        };
                        return pictureModel;
                    });

                    return subCatModel;
                })
                .ToList()
            );




            //featured products
            if (!_catalogSettings.IgnoreFeaturedProducts)
            {
                //We cache a value indicating whether we have featured products
                IPagedList<Product> featuredProducts = null;
                string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_HAS_FEATURED_PRODUCTS_KEY, categoryId,
                    string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()), _storeContext.CurrentStore.Id);
                var hasFeaturedProductsCache = _cacheManager.Get<bool?>(cacheKey);
                if (!hasFeaturedProductsCache.HasValue)
                {
                    //no value in the cache yet
                    //let's load products and cache the result (true/false)
                    featuredProducts = _productService.SearchProducts(
                       categoryIds: new List<int> { category.Id },
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: true);
                    hasFeaturedProductsCache = featuredProducts.TotalCount > 0;
                    _cacheManager.Set(cacheKey, hasFeaturedProductsCache, 60);
                }
                if (hasFeaturedProductsCache.Value && featuredProducts == null)
                {
                    //cache indicates that the category has featured products
                    //let's load them
                    featuredProducts = _productService.SearchProducts(
                       categoryIds: new List<int> { category.Id },
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: true);
                }
                if (featuredProducts != null)
                {
                    model.FeaturedProducts = PrepareProductOverviewModels(featuredProducts).ToList();
                }
            }
            var result = model.MapTo<CategoryModelApi, CategoryDetailFeaturedProductAndSubcategoryResponseModel>();
            return Ok(result);
        }

        #endregion

        #region manufacture
        [System.Web.Http.Route("api/homepagemanufacture")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult ManufacturerAll(int? thumbPictureSize = null)
        {
            var model = new List<MenufactureOverViewModelApi>();
            var manufacturers = _manufacturerService.GetAllManufacturers();
            foreach (var manufacturer in manufacturers)
            {
                var modelMan = manufacturer.MapTo<Manufacturer, MenufactureOverViewModelApi>();

                //prepare picture model
                int pictureSize = _mediaSettings.CategoryThumbPictureSize;
                //prepare picture model
                if (thumbPictureSize.HasValue)
                {
                    pictureSize = thumbPictureSize.GetValueOrDefault();
                }
                var manufacturerPictureCacheKey = string.Format(ModelCacheEventConsumer.MANUFACTURER_PICTURE_MODEL_KEY, manufacturer.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                modelMan.DefaultPictureModel = _cacheManager.Get(manufacturerPictureCacheKey, () =>
                {
                    var picture = _pictureService.GetPictureById(manufacturer.PictureId);
                    var pictureModel = new PictureModel
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize)
                    };
                    return pictureModel;
                });
                model.Add(modelMan);
            }

            var result = new GeneralResponseModel<List<MenufactureOverViewModelApi>>()
            {
                Data = model
            };
            return Ok(result);
        }
        [System.Web.Http.Route("api/Manufacturer/{manufacturerId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult Manufacturer(int manufacturerId, int pageNumber = 1, int orderBy = 0)
        {
            var manufacturer = _manufacturerService.GetManufacturerById(manufacturerId);
            if (manufacturer == null || manufacturer.Deleted)
                return NotFound();

            //Check whether the current user has a "Manage catalog" permission
            //It allows him to preview a manufacturer before publishing
            if (!manufacturer.Published && !_permissionService.Authorize(StandardPermissionProvider.ManageManufacturers))
                return NotFound();

            //ACL (access control list)
            if (!_aclService.Authorize(manufacturer))
                return NotFound();

            //Store mapping
            if (!_storeMappingService.Authorize(manufacturer))
                return NotFound();

            //'Continue shopping' URL
            //_genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
            //    SystemCustomerAttributeNames.LastContinueShoppingPage,
            //    _webHelper.GetThisPageUrl(false),
            //    _storeContext.CurrentStore.Id);

            var model = manufacturer.ToModel();
            PrepareSortingOptions(model.PagingFilteringContext, orderBy);

            var selectedPriceRange = model.PagingFilteringContext.PriceRangeFilter.GetSelectedPriceRange(_webHelper, manufacturer.PriceRanges);
            decimal? minPriceConverted = null;
            decimal? maxPriceConverted = null;
            if (selectedPriceRange != null)
            {
                if (selectedPriceRange.From.HasValue)
                    minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.From.Value, _workContext.WorkingCurrency);

                if (selectedPriceRange.To.HasValue)
                    maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.To.Value, _workContext.WorkingCurrency);
            }


            //products
            IList<int> filterableSpecificationAttributeOptionIds;
            var products = _productService.SearchProducts(out filterableSpecificationAttributeOptionIds, true,
                manufacturerId: manufacturer.Id,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                featuredProducts: _catalogSettings.IncludeFeaturedProductsInNormalLists ? null : (bool?)false,
                priceMin: minPriceConverted,
                priceMax: maxPriceConverted,
                orderBy: (ProductSortingEnum)orderBy,
                pageIndex: pageNumber - 1,
                pageSize: this.pageSize);
            var price = _productServiceApi.SearchProductsPrice(
              manufacturerId: manufacturer.Id,
              storeId: _storeContext.CurrentStore.Id,
              visibleIndividuallyOnly: true,
              featuredProducts: _catalogSettings.IncludeFeaturedProductsInNormalLists ? null : (bool?)false,
              priceMin: minPriceConverted,
              priceMax: maxPriceConverted,
              orderBy: (ProductSortingEnum)orderBy,
              pageSize: this.pageSize,
              pageIndex: pageNumber - 1);
            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);

            var result = model.ToModel(price: price);
            return Ok(result);
            //template
        }

        [System.Web.Http.Route("api/manufacturerfeaturedproduct/{manufacturerId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult ManufacturerFeaturedProduct(int manufacturerId)
        {
            var manufacturer = _manufacturerService.GetManufacturerById(manufacturerId);
            if (manufacturer == null || manufacturer.Deleted)
                return NotFound();

            //Check whether the current user has a "Manage catalog" permission
            //It allows him to preview a manufacturer before publishing
            if (!manufacturer.Published && !_permissionService.Authorize(StandardPermissionProvider.ManageManufacturers))
                return NotFound();

            //ACL (access control list)
            if (!_aclService.Authorize(manufacturer))
                return NotFound();

            //Store mapping
            if (!_storeMappingService.Authorize(manufacturer))
                return NotFound();

            var model = manufacturer.ToModel();



            //featured products
            if (!_catalogSettings.IgnoreFeaturedProducts)
            {
                IPagedList<Product> featuredProducts = null;

                //We cache a value indicating whether we have featured products
                string cacheKey = string.Format(ModelCacheEventConsumer.MANUFACTURER_HAS_FEATURED_PRODUCTS_KEY,
                    manufacturerId,
                    string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                    _storeContext.CurrentStore.Id);
                var hasFeaturedProductsCache = _cacheManager.Get<bool?>(cacheKey);
                if (!hasFeaturedProductsCache.HasValue)
                {
                    //no value in the cache yet
                    //let's load products and cache the result (true/false)
                    featuredProducts = _productService.SearchProducts(
                       manufacturerId: manufacturer.Id,
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: true);
                    hasFeaturedProductsCache = featuredProducts.TotalCount > 0;
                    _cacheManager.Set(cacheKey, hasFeaturedProductsCache, 60);
                }
                if (hasFeaturedProductsCache.Value && featuredProducts == null)
                {
                    //cache indicates that the manufacturer has featured products
                    //let's load them
                    featuredProducts = _productService.SearchProducts(
                       manufacturerId: manufacturer.Id,
                       storeId: _storeContext.CurrentStore.Id,
                       visibleIndividuallyOnly: true,
                       featuredProducts: true);
                }
                if (featuredProducts != null)
                {
                    model.FeaturedProducts = PrepareProductOverviewModels(featuredProducts).ToList();
                }
            }

            var result = model.MapTo<ManuFactureModelApi, ManufacturerDetailFeaturedProductResponseModel>();
            return Ok(result);
        }

        [System.Web.Http.Route("api/catalog/search")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult Search(SearchModelApi model, int pageNumber = 1)
        {
            //'Continue shopping' URL
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.LastContinueShoppingPage,
                _webHelper.GetThisPageUrl(false),
                _storeContext.CurrentStore.Id);
            var price=new PriceRange();
            if (model == null)
            {
                model = new SearchModelApi();
                
            }
                

            var searchTerms = model.q;
            if (searchTerms == null)
                searchTerms = "";
            searchTerms = searchTerms.Trim();


            IPagedList<Product> products = new PagedList<Product>(new List<Product>(), 0, 1);
            // only search if query string search keyword is set (used to avoid searching or displaying search term min length error message on /search page load)

            if (searchTerms.Length < _catalogSettings.ProductSearchTermMinimumLength)
            {
                model.Warning = string.Format(_localizationService.GetResource("Search.SearchTermMinimumLengthIsNCharacters"), _catalogSettings.ProductSearchTermMinimumLength);
            }
            else
            {
                var categoryIds = new List<int>();
                int manufacturerId = 0;
                decimal? minPriceConverted = null;
                decimal? maxPriceConverted = null;
                bool searchInDescriptions = false;
                if (!string.IsNullOrEmpty(model.pf) || !string.IsNullOrEmpty(model.pt))
                {
                    model.adv = true;
                }

                if (model.adv)
                {
                    //advanced search
                    var categoryId = model.cid;
                    if (categoryId > 0)
                    {
                        categoryIds.Add(categoryId);
                        if (model.isc)
                        {
                            //include subcategories
                            categoryIds.AddRange(GetChildCategoryIds(categoryId));
                        }
                    }


                    manufacturerId = model.mid;

                    //min price
                    if (!string.IsNullOrEmpty(model.pf))
                    {
                        decimal minPrice;
                        if (decimal.TryParse(model.pf, out minPrice))
                            minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(minPrice, _workContext.WorkingCurrency);
                    }
                    //max price
                    if (!string.IsNullOrEmpty(model.pt))
                    {
                        decimal maxPrice;
                        if (decimal.TryParse(model.pt, out maxPrice))
                            maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(maxPrice, _workContext.WorkingCurrency);
                    }

                    searchInDescriptions = model.sid;
                }

                //var searchInProductTags = false;
                var searchInProductTags = searchInDescriptions;

                //products
                products = _productService.SearchProducts(
                    categoryIds: categoryIds,
                    manufacturerId: manufacturerId,
                    storeId: _storeContext.CurrentStore.Id,
                    visibleIndividuallyOnly: true,
                    priceMin: minPriceConverted,
                    priceMax: maxPriceConverted,
                    keywords: searchTerms,
                    searchDescriptions: searchInDescriptions,
                    searchSku: searchInDescriptions,
                    searchProductTags: searchInProductTags,
                    languageId: _workContext.WorkingLanguage.Id,
                    orderBy: (ProductSortingEnum)0,
                    pageIndex: pageNumber - 1,
                    pageSize: this.pageSize);


                 price = _productServiceApi.SearchProductsPrice(
                    categoryIds: categoryIds,
                    manufacturerId: manufacturerId,
                    storeId: _storeContext.CurrentStore.Id,
                    visibleIndividuallyOnly: true,
                    priceMin: minPriceConverted,
                    priceMax: maxPriceConverted,
                    keywords: searchTerms,
                    searchDescriptions: searchInDescriptions,
                    searchSku: searchInDescriptions,
                    searchProductTags: searchInProductTags,
                    languageId: _workContext.WorkingLanguage.Id,
                    orderBy: (ProductSortingEnum)0,
                    pageIndex: pageNumber - 1,
                    pageSize: this.pageSize);
                model.Products = PrepareProductOverviewModels(products).ToList();

                model.NoResults = !model.Products.Any();

                //search term statistics
                if (!String.IsNullOrEmpty(searchTerms))
                {
                    var searchTerm = _searchTermService.GetSearchTermByKeyword(searchTerms, _storeContext.CurrentStore.Id);
                    if (searchTerm != null)
                    {
                        searchTerm.Count++;
                        _searchTermService.UpdateSearchTerm(searchTerm);
                    }
                    else
                    {
                        searchTerm = new SearchTerm
                        {
                            Keyword = searchTerms,
                            StoreId = _storeContext.CurrentStore.Id,
                            Count = 1
                        };
                        _searchTermService.InsertSearchTerm(searchTerm);
                    }
                }

                //event
                _eventPublisher.Publish(new ProductSearchEvent
                {
                    SearchTerm = searchTerms,
                    SearchInDescriptions = searchInDescriptions,
                    CategoryIds = categoryIds,
                    ManufacturerId = manufacturerId,
                    WorkingLanguageId = _workContext.WorkingLanguage.Id
                });
            }


            model.PagingFilteringContext.LoadPagedList(products);
            var result = model.ToModel(price: price);
            return Ok(result);
        }

        [System.Web.Http.Route("api/searchtermautocomplete/{searchkeyword}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult SearchTermAutoComplete(string searchkeyword)
        {
           
                //var categories = _categoryServiceapi.GetAllAutoCompleteCategories(term,
                //    0, 25, false).ToList();
                string categoryCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_MENU_MODEL_KEY,
                    _workContext.WorkingLanguage.Id,
                    string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                    _storeContext.CurrentStore.Id);
                var categories = _cacheManager.Get(categoryCacheKey, () => _categoryServiceapi.GetAllAutoCompleteCategories(searchkeyword,
                    0, 25, false));
                var model = PrepareSearchTermAutoCompleteApiModel(categories).ToList();
                var result = new SearchTermAutoCompleteResponceModel<SearchTermAutoCompleteApiModel>()
                {
                    Categories = model
                };

                return Ok(result);
            
        }

        

        [System.Web.Http.Route("api/Tag/{productTagId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult ProductsByTag(int productTagId, int pageNumber = 1, int orderBy = 0)
        {
            var productTag = _productTagService.GetProductTagById(productTagId);
            if (productTag == null)
                return NotFound();

            var model = productTag.ToModel();

            
            //sorting
            PrepareSortingOptions(model.PagingFilteringContext, orderBy);
            //view mode
            


            //products
            var products = _productService.SearchProducts(
                storeId: _storeContext.CurrentStore.Id,
                productTagId: productTag.Id,
                visibleIndividuallyOnly: true,
                orderBy: (ProductSortingEnum)orderBy,
                pageIndex: pageNumber - 1,
                pageSize: this.pageSize);
           
            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);
            var result = model.ToModel();
            return Ok(result);
        }


        //public IHttpActionResult Vendor(int vendorId, int pageNumber = 1, int orderBy = 0)
        //{
        //    var vendor = _vendorService.GetVendorById(vendorId);
        //    if (vendor == null || vendor.Deleted || !vendor.Active)
        //        return NotFound();

        //    //Vendor is active?
        //    if (!vendor.Active)
        //        return NotFound();

        //    //'Continue shopping' URL
        //    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
        //        SystemCustomerAttributeNames.LastContinueShoppingPage,
        //        _webHelper.GetThisPageUrl(false),
        //        _storeContext.CurrentStore.Id);

        //    var model = vendor.ToModel(_vendorSettings);


        //    //sorting
        //    PrepareSortingOptions(model.PagingFilteringContext, orderBy);
           
        //    //products
        //    IList<int> filterableSpecificationAttributeOptionIds;
        //    var products = _productService.SearchProducts(out filterableSpecificationAttributeOptionIds, true,
        //        vendorId: vendor.Id,
        //        storeId: _storeContext.CurrentStore.Id,
        //        visibleIndividuallyOnly: true,
        //        orderBy: (ProductSortingEnum)orderBy,
        //        pageIndex: pageNumber - 1,
        //        pageSize: this.pageSize);
        //    model.Products = PrepareProductOverviewModels(products).ToList();

        //    model.PagingFilteringContext.LoadPagedList(products);

        //    var result = model.ToModel();
        //    return Ok(result);
        //}
        #endregion

        #region Vendors

        [System.Web.Http.Route("api/vendor/{vendorId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult Vendor(int vendorId)
        {
            var vendor = _vendorService.GetVendorById(vendorId);
           
            //'Continue shopping' URL
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.LastContinueShoppingPage,
                _webHelper.GetThisPageUrl(false),
                _storeContext.CurrentStore.Id);

            var model = new VendorDetailProductResponseModel
            {
                Id = vendor.Id,
                Name = vendor.GetLocalized(x => x.Name),
                Description = vendor.GetLocalized(x => x.Description),
                MetaKeywords = vendor.GetLocalized(x => x.MetaKeywords),
                MetaDescription = vendor.GetLocalized(x => x.MetaDescription),
                MetaTitle = vendor.GetLocalized(x => x.MetaTitle),
                SeName = vendor.GetSeName(),
                AllowCustomersToContactVendors = _vendorSettings.AllowCustomersToContactVendors
            };
            //prepare picture model
            int pictureSize = _mediaSettings.VendorThumbPictureSize;
            var pictureCacheKey = string.Format(ModelCacheEventConsumer.VENDOR_PICTURE_MODEL_KEY, vendor.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            model.PictureModel = _cacheManager.Get(pictureCacheKey, () =>
            {
                var picture = _pictureService.GetPictureById(vendor.PictureId);
                var pictureModel = new PictureModel
                {
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                    Title = string.Format(_localizationService.GetResource("Media.Vendor.ImageLinkTitleFormat"), model.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Vendor.ImageAlternateTextFormat"), model.Name)
                };
                return pictureModel;
            });

            var customerVendor = _customerServiceApi.GetCustomerByVendorId(vendor.Id);
            model.LogoModel = _cacheManager.Get(pictureCacheKey, () =>
            {
                var picture = _pictureService.GetPictureById(customerVendor.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId));
                var pictureModel = new PictureModel
                {
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                    Title = string.Format(_localizationService.GetResource("Media.Vendor.ImageLinkTitleFormat"), model.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Vendor.ImageAlternateTextFormat"), model.Name)
                };
                return pictureModel;
            });


            //products
            IList<int> filterableSpecificationAttributeOptionIds;
            var products = _productService.SearchProducts(out filterableSpecificationAttributeOptionIds, true,
                vendorId: vendor.Id,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true);

            model.Products = PrepareProductOverviewModels(products).ToList();

            return Ok(model);
        }

        [System.Web.Http.Route("api/vendorall")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult VendorAll()
        {  
            var result = new GeneralResponseModel<List<VendorModel>>();
            var model = new List<VendorModel>();
            var vendors = _vendorService.GetAllVendors();
            foreach (var vendor in vendors)
            {
                var vendorModel = new VendorModel
                {
                    Id = vendor.Id,
                    Name = vendor.GetLocalized(x => x.Name),
                    Description = vendor.GetLocalized(x => x.Description),
                    MetaKeywords = vendor.GetLocalized(x => x.MetaKeywords),
                    MetaDescription = vendor.GetLocalized(x => x.MetaDescription),
                    MetaTitle = vendor.GetLocalized(x => x.MetaTitle),
                    SeName = vendor.GetSeName(),
                    AllowCustomersToContactVendors = _vendorSettings.AllowCustomersToContactVendors
                };
                //prepare picture model
                int pictureSize = _mediaSettings.VendorThumbPictureSize;
                var pictureCacheKey = string.Format(ModelCacheEventConsumer.VENDOR_PICTURE_MODEL_KEY, vendor.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                vendorModel.PictureModel = _cacheManager.Get(pictureCacheKey, () =>
                {
                    var picture = _pictureService.GetPictureById(vendor.PictureId);
                    var pictureModel = new PictureModel
                    {
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        Title = string.Format(_localizationService.GetResource("Media.Vendor.ImageLinkTitleFormat"), vendorModel.Name),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Vendor.ImageAlternateTextFormat"), vendorModel.Name)
                    };
                    return pictureModel;
                });

                var customerVendor = _customerServiceApi.GetCustomerByVendorId(vendor.Id);
                vendorModel.LogoModel = _cacheManager.Get(pictureCacheKey, () =>
                {
                    var picture = _pictureService.GetPictureById(customerVendor.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId));
                    var pictureModel = new PictureModel
                    {
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        Title = string.Format(_localizationService.GetResource("Media.Vendor.ImageLinkTitleFormat"), vendorModel.Name),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Vendor.ImageAlternateTextFormat"), vendorModel.Name)
                    };
                    return pictureModel;
                });

                model.Add(vendorModel);
            }

            result.Data = model;

            return Ok(result);
        }

        [System.Web.Http.Route("api/vendornav")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult VendorNavigation()
        {
            string cacheKey = ModelCacheEventConsumer.VENDOR_NAVIGATION_MODEL_KEY;
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var vendors = _vendorService.GetAllVendors(pageSize: _vendorSettings.VendorsBlockItemsToDisplay);
                var model = new VendorNavigationModel
                {
                    TotalVendors = vendors.TotalCount
                };

                foreach (var vendor in vendors)
                {
                    model.Vendors.Add(new VendorBriefInfoModel
                    {
                        Id = vendor.Id,
                        Name = vendor.GetLocalized(x => x.Name),
                        SeName = vendor.GetSeName(),
                    });
                }
                return model;
            });

            return Ok(cacheModel);
        }

        #endregion

        #region OnSale

        // On Sale

        [System.Web.Http.Route("api/onsaleall")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult OnSalesAll(int? thumbPictureSize = null, int pageNumber = 1, int pageSize = 25)
        {

            #region Comment

            //var allCategories = _categoryService.GetAllCategories().Where(w=>w.ParentCategoryId == 0).ToList();

            //OnSaleModel saleModel = new OnSaleModel();

            //foreach (var category in allCategories)
            //{   

            //    var model = category.ToModel();

            //    var categoryIds = new List<int>();
            //    categoryIds.Add(category.Id);
            //    if (_catalogSettings.ShowProductsFromSubcategories)
            //    {
            //        //include subcategories
            //        categoryIds.AddRange(GetChildCategoryIds(category.Id));
            //    }

            //    // sub categories
            //    //subcategories
            //    string subCategoriesCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_SUBCATEGORIES_KEY,
            //        category.Id,
            //        string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
            //        _storeContext.CurrentStore.Id,
            //        _workContext.WorkingLanguage.Id,
            //        _webHelper.IsCurrentConnectionSecured());
            //    model.SubCategories = _cacheManager.Get(subCategoriesCacheKey, () =>
            //        _categoryService.GetAllCategoriesByParentCategoryId(category.Id)
            //        .Select(x =>
            //        {
            //            var subCatModel = new SubCategoryModelApi
            //            {
            //                Id = x.Id,
            //                Name = x.GetLocalized(y => y.Name)
            //            };

            //            //prepare picture model
            //            int pictureSize = 0;
            //            if (thumbPictureSize == null)
            //            {
            //                pictureSize = _mediaSettings.CategoryThumbPictureSize;
            //            }
            //            else
            //            {
            //                pictureSize = (int)thumbPictureSize;
            //            }
            //            var subCategoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, x.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            //            subCatModel.PictureModel = _cacheManager.Get(subCategoryPictureCacheKey, () =>
            //            {
            //                var picture = _pictureService.GetPictureById(x.PictureId);
            //                var pictureModel = new PictureModel
            //                {
            //                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
            //                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
            //                    Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), subCatModel.Name),
            //                    AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), subCatModel.Name)
            //                };
            //                return pictureModel;
            //            });

            //            return subCatModel;
            //        })
            //        .ToList()
            //    );
            //    // prepare category picture model
            //    int cpictureSize = 0;
            //    if (thumbPictureSize == null)
            //    {
            //        cpictureSize = _mediaSettings.CategoryThumbPictureSize;
            //    }
            //    else
            //    {
            //        cpictureSize = (int)thumbPictureSize;
            //    }
            //    var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, model.Id, cpictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            //    model.PictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
            //    {
            //        var picture = _pictureService.GetPictureById(category.PictureId);
            //        var pictureModel = new PictureModel
            //        {
            //            FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
            //            ImageUrl = _pictureService.GetPictureUrl(picture, cpictureSize),
            //            Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), model.Name),
            //            AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), model.Name)
            //        };
            //        return pictureModel;
            //    });

            //    //products              
            //    var products = _productServiceApi.SearchOnSaleProducts(                   
            //        categoryIds: categoryIds,
            //        storeId: _storeContext.CurrentStore.Id,
            //        visibleIndividuallyOnly: true,
            //        pageIndex: pageIndex,
            //        pageSize: pageSize);

            //    model.Products = PrepareProductOverviewModels(products).ToList();

            //   // model.ToModel(price: price);
            //    model.ProductCount = products.Count;

            //    saleModel.categorySaleModels.Add(model);

            //}

            #endregion

            if (!thumbPictureSize.HasValue)
            {
                thumbPictureSize = _mediaSettings.ProductThumbPictureSize;
            }

            IList<int> filterableSpecificationAttributeOptionIds;

            var products = _productServiceApi.SearchProductsOnApi(out filterableSpecificationAttributeOptionIds,true,
                visibleIndividuallyOnly: true, pageIndex: pageNumber- 1, pageSize: pageSize);
            var productspages = new PagedList<Product>(products, pageNumber - 1, pageSize);
            OnSaleProductModel model = new OnSaleProductModel();

            model.Products = PrepareProductOverviewModels(products, true, true, thumbPictureSize).ToList();

            model.SubCategories = PrepareCategoryFilterOnSale(products, (int)thumbPictureSize).ToList();

            var result = new OnSaleProductResponseModel<OnSaleProductModel>()
            {
                Products = model.Products,
                SubCategories = model.SubCategories,
                TotalPages = productspages.TotalPages
            };

            return Ok(result);
        }

        [System.Web.Http.Route("api/onsalecategory/{categoryId}")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult OnSalesCategory(int categoryId, int? thumbPictureSize = null, int pageNumber = 1, int pageSize = 25)
        {
            var category = _categoryService.GetCategoryById(categoryId);
            var model = category.ToModel();

            var categoryIds = new List<int>();
            categoryIds.Add(category.Id);
            if (_catalogSettings.ShowProductsFromSubcategories)
            {
                //include subcategories
                categoryIds.AddRange(GetChildCategoryIds(category.Id));
            }

            // sub categories
            //subcategories
            string subCategoriesCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_SUBCATEGORIES_KEY,
                category.Id,
                string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                _storeContext.CurrentStore.Id,
                _workContext.WorkingLanguage.Id,
                _webHelper.IsCurrentConnectionSecured());
            model.SubCategories = _cacheManager.Get(subCategoriesCacheKey, () =>
                _categoryService.GetAllCategoriesByParentCategoryId(category.Id)
                .Select(x =>
                {
                    var subCatModel = new SubCategoryModelApi
                    {
                        Id = x.Id,
                        Name = x.GetLocalized(y => y.Name)
                    };

                    //prepare picture model
                    int pictureSize = 0;
                    if (thumbPictureSize == null)
                    {
                        pictureSize = _mediaSettings.CategoryThumbPictureSize;
                    }
                    else
                    {
                        pictureSize = (int)thumbPictureSize;
                    }
                    var subCategoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, x.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                    subCatModel.PictureModel = _cacheManager.Get(subCategoryPictureCacheKey, () =>
                    {
                        var picture = _pictureService.GetPictureById(x.PictureId);
                        var pictureModel = new PictureModel
                        {
                            FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                            ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                            Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), subCatModel.Name),
                            AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), subCatModel.Name)
                        };
                        return pictureModel;
                    });

                    return subCatModel;
                })
                .ToList()
            );

            // prepare category picture model
            int cpictureSize = 0;
            if (thumbPictureSize == null)
            {
                cpictureSize = _mediaSettings.CategoryThumbPictureSize;
            }
            else
            {
                cpictureSize = (int)thumbPictureSize;
            }
            var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, model.Id, cpictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            model.PictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
            {
                var picture = _pictureService.GetPictureById(category.PictureId);
                var pictureModel = new PictureModel
                {
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    ImageUrl = _pictureService.GetPictureUrl(picture, cpictureSize),
                    Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), model.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), model.Name)
                };
                return pictureModel;
            });
            IList<int> filterableSpecificationAttributeOptionIds;
            //products              
            var products = _productServiceApi.SearchProductsOnApi(
                out filterableSpecificationAttributeOptionIds,true,
                categoryIds: categoryIds,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                pageIndex: pageNumber - 1,
                pageSize: pageSize);

            model.Products = PrepareProductOverviewModels(products).ToList();
            var productspages = new PagedList<Product>(products, pageNumber - 1, pageSize);
            // model.ToModel(price: price);
            model.ProductCount = products.Count;
            var result = new OnSaleCategoryResponseModel<CategoryModelApi>()
            {
                Description = model.Description,
                MetaKeywords = model.MetaKeywords,
                MetaDescription = model.MetaDescription,
                MetaTitle = model.MetaTitle,
                SeName = model.SeName,
                PictureModel = model.PictureModel,
                PagingFilteringContext = model.PagingFilteringContext,
                SubCategories = model.SubCategories,
                Products = model.Products,
                FeaturedProducts = model.FeaturedProducts,
                Tags = model.Tags,
                Id = model.Id,
                Name = model.Name,
                ProductCount = model.ProductCount,
                TotalPages = productspages.TotalPages
            };
            

            return Ok(result);
        }

        [System.Web.Http.Route("api/onsaleweekly")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult OnSalesWeekly(int? thumbPictureSize = null, int pageNumber = 1, int pageSize = 25)
        {
            if (!thumbPictureSize.HasValue)
            {
                thumbPictureSize = _mediaSettings.ProductThumbPictureSize;
            }

            IList<int> filterableSpecificationAttributeOptionIds;

            var products = _productServiceApi.SearchProductsOnApi(out filterableSpecificationAttributeOptionIds,true,
                visibleIndividuallyOnly: true, hasTierPrice: false, hasDiscountApplied: false, pageIndex: pageNumber - 1, pageSize: pageSize);
            var productspages = new PagedList<Product>(products, pageNumber - 1, pageSize);
            OnSaleProductModel model = new OnSaleProductModel();

            model.Products = PrepareProductOverviewModels(products, true, true, thumbPictureSize).ToList();

            model.SubCategories = PrepareCategoryFilterOnSale(products, (int)thumbPictureSize).ToList();

            var result = new OnSaleProductResponseModel<OnSaleProductModel>()
            {
                Products = model.Products,
                SubCategories = model.SubCategories,
                TotalPages = productspages.TotalPages
            };

            return Ok(result);
        }

        [System.Web.Http.Route("api/onsaletopdeals")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult OnSalesTopDeals(int? thumbPictureSize = null, int pageNumber = 1, int pageSize = 25)
        {
            if (!thumbPictureSize.HasValue)
            {
                thumbPictureSize = _mediaSettings.ProductThumbPictureSize;
            }

            //load and cache report
            var report = _cacheManager.Get(string.Format(ModelCacheEventConsumer.HOMEPAGE_BESTSELLERS_IDS_KEY, _storeContext.CurrentStore.Id),
                () =>
                    _orderReportService.BestSellersReport(storeId: _storeContext.CurrentStore.Id,
                    pageSize: pageSize));


            //load products
            var products = _productServiceApi.GetTopDealsProductsByIds(report.Select(x => x.ProductId).ToArray());
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();
            var productspages = new PagedList<Product>(products, pageNumber - 1, pageSize);
            OnSaleProductModel model = new OnSaleProductModel();
            //prepare model
            model.Products = PrepareProductOverviewModels(products, true, true, thumbPictureSize).ToList();
            model.SubCategories = PrepareCategoryFilterOnSale(products, (int)thumbPictureSize).ToList();

            var result = new OnSaleProductResponseModel<OnSaleProductModel>()
            {
                Products = model.Products,
                SubCategories = model.SubCategories,
                TotalPages = productspages.TotalPages
            };

            return Ok(result);
        }

        #endregion

        #endregion
    }
}
