using System.Collections.Generic;
using System.IO;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Plugins;
using Nop.Plugin.Misc.BsWebApi.Data;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Common;
using Nop.Services.Vendors;
using Nop.Web.Framework.Menu;
using Nop.Plugin.Misc.BsWebApi.Services;
using System.Linq;

namespace Nop.Plugin.Misc.BsWebApi
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class BsWebApiPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin, IWidgetPlugin
    {
        #region Fields

        private readonly IBsNopMobilePluginService _BsPluginService;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly BswebApiObjectContext _objectContext;
        private readonly IContentManagementTemplateService _contentmanagementTemplate;
        private readonly BswebApiObjectContext _context;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor
        public BsWebApiPlugin(IBsNopMobilePluginService BsPluginService,
            ISettingService settingService,
            IWorkContext workContext,
            BsNopMobileSettings webapiSettings,
            ILocalizationService localizationService,
             IContentManagementTemplateService contentmanagementTemplate, BswebApiObjectContext context)
        {
            this._BsPluginService = BsPluginService;
            this._settingService = settingService;
            this._workContext = workContext;
            this._contentmanagementTemplate = contentmanagementTemplate;
            this._context = context;
            this._localizationService = localizationService;
        }

        #endregion

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                Title = "BS WebApi Configure",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };

            var configurenode = new SiteMapNode()
            {
                ControllerName = "Admin/Widget",
                ActionName = "ConfigureWidget",
                Title = "Overview",
                RouteValues = new RouteValueDictionary() { { "systemName", "BrainStation.BsWebApi" }, { "area", null } },
                Visible = true
            };
            menuItem.ChildNodes.Add(configurenode);

            var menuFirstItem = new SiteMapNode()
            {
                Title = "General Settings",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "GeneralSetting",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(menuFirstItem);

            var menuSecondItem = new SiteMapNode()
            {
                Title = "Theme Personalization",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "Theme",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(menuSecondItem);

            var menuTherdItem = new SiteMapNode()
            {
                Title = "Banner Sliders",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "BannerSlider",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(menuTherdItem);

            var menuforthItem = new SiteMapNode()
            {
                Title = "Contant Management",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "ContentManagement",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(menuforthItem);

            var menufifthItem = new SiteMapNode()
            {
                Title = "Push Notification",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "PushNotification",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(menufifthItem);

            var menusixthItem = new SiteMapNode()
            {
                Title = "Category Icons",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "CategoryIcons",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            var menuseventhItem = new SiteMapNode()
            {
                Title = "Mobile WebSite Setting",
                ControllerName = "BsWebApiConfiguration",
                ActionName = "MobileWebSiteSetting",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(menusixthItem);

            var contactInfoItem = new SiteMapNode()
            {
                Title = _localizationService.GetResource("Plugins.NopMobile.WebApi.contactInfo"),
                ControllerName = "BsWebApiConfiguration",
                ActionName = "ContactInfo",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(contactInfoItem);

            var sliderInfoItem = new SiteMapNode()
            {
                Title = _localizationService.GetResource("Plugins.NopMobile.WebApi.Slider"),
                ControllerName = "BsWebApiConfiguration",
                ActionName = "SliderImage",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            menuItem.ChildNodes.Add(sliderInfoItem);


            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItem);
            else
                rootNode.ChildNodes.Add(menuItem);
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "BsWebApiConfiguration";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Misc.BsWebApi.Controllers" }, { "area", null } };
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>() { "admin_webapi_bottom" };

        }
        public bool Authenticate()
        {
            return true;
        }


        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Dashboard";
            controllerName = "BsWebApiConfiguration";
            routeValues = new RouteValueDictionary()
            {
                {"Namespaces", "Nop.Plugin.Misc.BsWebApi.Controllers"},
                {"area", null},
                {"widgetZone", widgetZone}
            };
        }

        public override void Install()
        {
            //settings
            var settings = new BsNopMobileSettings()
            {
                //ProductPictureSize = 125,
                //PassShippingInfo = false,
                //StaticFileName = string.Format("froogle_{0}.xml", CommonHelper.GenerateRandomDigitCode(10)),
            };
            _settingService.SaveSetting(settings);

            #region Local Resources

            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.NopMobilemenuMainTitle", "NopMobileWebApiConfigration");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Willusedefaultnopcategory", "Will use default nopcategory?");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.DefaultNopFlowSameAs", "Will use default nopflow same as?");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.SubCategory", "Select a Sub Category");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.CategoryIcon", "Upload Category Icon");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.ActivatePushNotification", "Activate push notification");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.SandboxMode", "Sandbox mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.GoogleApiProjectNumber", "Google api project number");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.UploudeIOSPEMFile", "Uploude IOS PEMFile");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.PEMPassword", "PEM password");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppNameOnGooglePlayStore", "App name on google playstore");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppUrlOnGooglePlayStore", "App URL on google playstore");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppNameOnAppleStore", "App name on Applestore");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppUrlonAppleStore", "App URL on Applestore");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppDescription", "App description");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppImage", "App image");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppLogo", "App Logo");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppLogoAltText", "App Logo Alternate Text");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.GcmApiKey", "Gcm Apikey");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppKey", "AppKey");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AppName", "App name");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.LicenceType", "Licence type");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.CreatedDate", "Created date");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AndroidAppStatus", "Android app status");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.DownloadUrl", "Download url");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.iOsAPPUDID", "Ios APPUDID");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.MobilWebsiteURL", "Mobil website URL");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.PushNotificationHeading", "Push notification heading");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.PushNotificationMessage", "Push notification message");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderBackgroundColor", "Header background color");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderFontandIconColor", "Header font and icon color");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.HighlightedTextColor", "High lighted text color");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.PrimaryTextColor", "Primary text color");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.SecondaryTextColor", "Secondary text color");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofPrimaryButton", "Background color of primary button");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.TextColorofPrimaryButton", "Text color of primary Button");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofSecondaryButton", "Background color of secondary button");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.menu", "NopMobile Menu");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.overview", "Overview");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.generalsetting", "General Settings");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.themepersonalization", "Theme Personalization");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.bannericon", "Banner Slide");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.contantmanegement", "Content Management");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.pushnotification", "Push Notification");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.categoryicon", "Category Icon");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.mobilewebsitesetting", "Mobile Website Settings");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderBackgroundColorHint", "Configure background color for heaer of the app");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderFontandIconColorHint", "Configure font and icon color for heaer of the app");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.HighlightedTextColorHint", "Configure font color for highlighted text message such warning,product,price etc.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.PrimaryTextColorHint", "Configure font color for primary text labels such as product name,category name etc.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.SecondaryTextColorHint", "Configure font color for secondary text labels such as option name,no. of rating etc.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofPrimaryButtonHint", "Configure background color for all primary button such as Add to Cart etc");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.TextColorofPrimaryButtonHint", "Configure font color for all primary button such as Add to Cart etc");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofSecondaryButtonHint", "Configure background color for all secondary button");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.persionalizewebsiteTitle", "Personalization [Website]");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.categoryiconTitle", "Configure Category Icon");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.configureTitle", "Configure Plugin");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.contentmanagementTitle", "ConfigureContent Management");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.bannerconfigureTitle", "ConfigureContent Banner");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.mobilewebsitesettingTitle", "Configure Mobile Settings");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.pushnotificationTitle", "Configure Push Notification");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.ContactNumber", "Contact Number");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.ContactEmail", "Contact Email");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.contactInfoTitle", "Configure Contact Information");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.contactInfo", "Contact Info");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture1", "Picture 1");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture2", "Picture 2");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture3", "Picture 3");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture4", "Picture 4");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture5", "Picture 5");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture", "Picture");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Picture.Hint", "Upload picture.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Text", "Comment");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Text.Hint", "Enter comment for picture. Leave empty if you don't want to display any text.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Link", "URL");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Link.Hint", "Enter URL. Leave empty if you don't want this picture to be clickable.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.IsProduct", "Is Product");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.IsProduct.Hint", "Checked, if you enter Product Id or Unchecked for Category Id.");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.ProdOrCat", "Product or Category Id");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.ProdOrCat.Hint", "Enter your product or category id");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.EnableBestseller", "Enable Bestseller Home Page");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.EnableFeaturedProducts", "Enable Featured Products Home Page");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.EnableNewProducts", "Enable New Products Home Page");
            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.AddNew", "Add New FeturedProduct");


            this.AddOrUpdatePluginLocaleResource("Plugins.NopMobile.WebApi.Slider", "Slider Setting");
            this.AddOrUpdatePluginLocaleResource("Slider.Products.Fields.StartDate", "Slider Image Active StartDate");
            this.AddOrUpdatePluginLocaleResource("Slider.Products.Fields.EndDate", "Slider Image Active EndDate");
            this.AddOrUpdatePluginLocaleResource("Slider.Products.Fields.ImageFor", "Slider Image For");

            this.AddOrUpdatePluginLocaleResource("Admin.BSSlider.Fields.ImageFor", "Slider Image For");
            this.AddOrUpdatePluginLocaleResource("Admin.BSSlider.Fields.SliderActiveStartDate", "Slider Image Active StartDate");
            this.AddOrUpdatePluginLocaleResource("Admin.BSSlider.Fields.SliderActiveEndDate", "Slider Image Active EndDate");
            this.AddOrUpdatePluginLocaleResource("Admin.Bsslider.Fields.IsProduct", "IsProduct");
            this.AddOrUpdatePluginLocaleResource("Admin.Bsslider.Fields.ProductOrCategory", "ProductId Or CategoryId");




            #endregion

            //#region Local Enums Resources

            //this.AddOrUpdatePluginLocaleResource("Enums.Nop.Plugin.Misc.BsWebApi.Models.Catalog.SortByList.All", "All");

            //#endregion

            //install db
            _context.Install();

            //base install
            base.Install();
        }
        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            #region Local Resources

            //settings

            _settingService.DeleteSetting<BsNopMobileSettings>();

            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.NopMobilemenuMainTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Willusedefaultnopcategory");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.SubCategory");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.CategoryIcon");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.DefaultNopFlowSameAs");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.ActivatePushNotification");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.SandboxMode");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.GoogleApiProjectNumber");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.UploudeIOSPEMFile");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.PEMPassword");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppNameOnGooglePlayStore");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppUrlOnGooglePlayStore");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppNameOnAppleStore");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppUrlonAppleStore");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppDescription");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppImage");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppLogo");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.GcmApiKey");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppKey");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppName");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.LicenceType");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.CreatedDate");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AndroidAppStatus");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.DownloadUrl");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.iOsAPPUDID");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.MobilWebsiteURL");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.PushNotificationHeading");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.PushNotificationMessage");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.ContactNumber");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.ContactEmail");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.contactInfoTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.contactInfo");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderBackgroundColor");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderFontandIconColor");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.HighlightedTextColor");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.PrimaryTextColor");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.SecondaryTextColor");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofPrimaryButton");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.TextColorofPrimaryButton");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofSecondaryButton");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.menu");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.overview");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.generalsetting");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.bannericon");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.contantmanegement");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.pushnotification");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.categoryicon");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.mobilewebsitesetting");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderBackgroundColorHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.HeaderFontandIconColorHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.HighlightedTextColorHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.PrimaryTextColorHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.SecondaryTextColorHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofPrimaryButtonHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.TextColorofPrimaryButtonHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.BackgroundColorofSecondaryButtonHint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.persionalizewebsiteTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.categoryiconTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.configureTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.contentmanagementTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.mobilewebsitesettingTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.pushnotificationTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture1");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture2");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture3");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture4");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture5");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Picture.Hint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Text");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Text.Hint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Link");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Link.Hint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.IsProduct");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.IsProduct.Hint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.ProdOrCat");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.ProdOrCat.Hint");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.bannerconfigureTitle");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.EnableBestseller");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.EnableFeaturedProducts");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.EnableNewProducts");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AddNew");
            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.AppLogoAltText");


            this.DeletePluginLocaleResource("Plugins.NopMobile.WebApi.Slider");
            this.DeletePluginLocaleResource("Slider.Products.Fields.StartDate");
            this.DeletePluginLocaleResource("Slider.Products.Fields.EndDate");
            this.DeletePluginLocaleResource("Slider.Products.Fields.ImageFor");

            this.DeletePluginLocaleResource("Admin.BSSlider.Fields.ImageFor");
            this.DeletePluginLocaleResource("Admin.BSSlider.Fields.SliderActiveStartDate");
            this.DeletePluginLocaleResource("Admin.BSSlider.Fields.SliderActiveEndDate");
            this.DeletePluginLocaleResource("Admin.Bsslider.Fields.IsProduct");
            this.DeletePluginLocaleResource("Admin.Bsslider.Fields.ProductOrCategory");
            #endregion

            _context.Uninstall();
            base.Uninstall();
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        /// 
       

    }
}
