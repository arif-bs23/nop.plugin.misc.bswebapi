﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.BsWebApi.Domain;
using System.Collections.Generic;
using Nop.Plugin.Misc.BsWebApi.Extensions;
using Nop.Services.Events;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
    public partial class BS_SliderService : IBS_SliderService
    {
        #region Field
        private readonly IRepository<BS_Slider> _bsSliderRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctr

        public BS_SliderService(IRepository<BS_Slider> bsSliderRepository, IEventPublisher eventPublisher)
        {
            _bsSliderRepository = bsSliderRepository;
            this._eventPublisher = eventPublisher;

        }

        #endregion

        #region Methods

        public void Delete(int Id)
        {
            //item.Deleted = true;

            var query = from c in _bsSliderRepository.Table
                        where c.Id == Id
            select c;

            var sliderImages = query.ToList();
            foreach (var sliderImage in sliderImages)
            {
                _bsSliderRepository.Delete(sliderImage);
            }
        }

        public void Insert(BS_Slider item)
        {
            _bsSliderRepository.Insert(item);
        }

        public BS_Slider GetBsSliderImageById(int Id)
        {
            return _bsSliderRepository.GetById(Id);
        }


        public IPagedList<BS_Slider> GetBSSliderImages(int pageIndex = 0, int pageSize = int.MaxValue) 
        {
            var query = from c in _bsSliderRepository.Table
                        select c;


            query = query.OrderBy(b => b.SliderActiveStartDate);

            var bsSliderImages = new PagedList<BS_Slider>(query, pageIndex, pageSize);
            return bsSliderImages;
        }


        public List<BS_Slider> GetBSSliderImagesByDate()
        {
            var bsSliderImages =
                _bsSliderRepository.Table.ToList()
                    .Where(c => DateTime.Now.Date.IsBetween(c.SliderActiveStartDate.Value, c.SliderActiveEndDate.Value))
                    .ToList();

            return bsSliderImages;
        }

        #endregion    
   
    
    

        public void Update(BS_Slider item)
        {
            _bsSliderRepository.Update(item);
        }
    }
}