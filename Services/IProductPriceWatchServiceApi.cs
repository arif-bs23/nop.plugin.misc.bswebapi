﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Misc.BsWebApi.Domain;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
   public partial interface IProductPriceWatchServiceApi
   {
       bool IsPriceWatchByCurrentCustomer(int productId, int customerId);
       bool InsertProductPriceWatch(BS_ProductPriceWatch productPriceWatch);
       bool UpdateProductPriceWatch(BS_ProductPriceWatch productPriceWatch);
       bool PriceWatchByCurrentCustomerIsExist(int productId, int customerId);
       IList<Product> PriceWatchProductListByCustomer(int customerId);
       IList<Customer> PriceWatchCustomerListByProductId(int productId);
   }
}
