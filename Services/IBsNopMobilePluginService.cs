﻿using Nop.Core;
using Nop.Plugin.Misc.BsWebApi.Domain;


namespace Nop.Plugin.Misc.BsWebApi.Services
{
    public partial interface IBsNopMobilePluginService
    {
        IPagedList<BS_FeaturedProducts> GetAllPluginFeatureProducts(int pageIndex = 0, int pageSize = int.MaxValue);

        BS_FeaturedProducts GetPluginFeatureProductsById(int FeatureProductsId);

        void InsertFeatureProducts(BS_FeaturedProducts FeatureProducts);

        void UpdateFeatureProducts(BS_FeaturedProducts FeatureProducts);

        void DeleteFeatureProducts(BS_FeaturedProducts FeatureProducts);
    }
}
