﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
   public partial   class CategoryServiceApi : ICategoryServiceApi
    {
        private readonly IRepository<Category> _categoryRepository;

       public CategoryServiceApi(IRepository<Category> categoryRepository)
       {
           this._categoryRepository = categoryRepository;
       }

       public virtual IPagedList<Category> GetAllAutoCompleteCategories(string searchkeyword = "",
           int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
       {
           var query = _categoryRepository.Table;
           query = query.Where(c => c.Name.Contains(searchkeyword));
           IList<Category> unsortedCategories = query.ToList();

           //sort categories
           var sortedCategories = SortCategoriesForTree(unsortedCategories);

           //paging
           return new PagedList<Category>(sortedCategories, pageIndex, pageSize);
       }

       public  IList<Category> SortCategoriesForTree(IList<Category> unsortedCategories, int parentId = 0, bool ignoreCategoriesWithoutExistingParent = false)
       {
           if (unsortedCategories == null)
               throw new ArgumentNullException("unsortedCategories");

           var result = new List<Category>();

           foreach (var cat in unsortedCategories.Where(c => c.ParentCategoryId == parentId).ToList())
           {
               result.Add(cat);
               result.AddRange(SortCategoriesForTree(unsortedCategories, cat.Id, true));
           }
           if (!ignoreCategoriesWithoutExistingParent && result.Count != unsortedCategories.Count)
           {
               //find categories without parent in provided category source and insert them into result
               foreach (var cat in unsortedCategories)
                   if (result.FirstOrDefault(x => x.Id == cat.Id) == null)
                       result.Add(cat);
           }
           return result;
       }

    }
}
