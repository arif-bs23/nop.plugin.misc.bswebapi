﻿using System;
using Nop.Core;
using Nop.Plugin.Misc.BsWebApi.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
    public partial interface IBS_SliderService
    {
        void Delete(int Id);
        void Insert(BS_Slider item);

        void Update(BS_Slider item);
        BS_Slider GetBsSliderImageById(int Id);

        IPagedList<BS_Slider> GetBSSliderImages(int pageIndex = 0, int pageSize = int.MaxValue);

        List<BS_Slider> GetBSSliderImagesByDate();

    }
}