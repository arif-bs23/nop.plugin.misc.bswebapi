﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
    public partial interface IManufacturerServiceApi
    {
        IPagedList<Manufacturer> GetManufacturersByCategoryId(string manufacturerName = "", int pageIndex = 0, int pageSize = 2147483647, bool showHidden = false);
    }
}
