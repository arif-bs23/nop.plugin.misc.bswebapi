﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Misc.BsWebApi.Domain;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
    public class ProductPriceWatchServiceServiceApi : IProductPriceWatchServiceApi
    {
         #region Fields

        private readonly IRepository<BS_ProductPriceWatch> _productPriceWatchRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Customer> _customerRepository;
        #endregion

        #region Ctor

        public ProductPriceWatchServiceServiceApi(
            IRepository<BS_ProductPriceWatch> productPriceWatchRepository,
            IRepository<Product> productRepository,
            IRepository<Customer> customerRepository)
        {
            this._productPriceWatchRepository = productPriceWatchRepository;
            this._productRepository = productRepository;
            this._customerRepository = customerRepository;
        }

        #endregion

        public bool IsPriceWatchByCurrentCustomer(int customerId, int productId)
        {
            return _productPriceWatchRepository.Table.Any(x => x.ProductId == productId && x.CustomerId == customerId && x.IsPriceWatch == true);
        }
        public IList<Product> PriceWatchProductListByCustomer(int customerId)
        {
             var productIds = (from productpricewatch in  _productPriceWatchRepository.Table
                          where productpricewatch.CustomerId == customerId && productpricewatch.IsPriceWatch == true
                          select productpricewatch.ProductId).ToArray();
            var products = from product in _productRepository.Table
                         where productIds.Contains(product.Id)
                         select product;
            return products.ToList();

        }

        public IList<Customer> PriceWatchCustomerListByProductId(int productId)
        {
            var customerIds = (from productpricewatch in _productPriceWatchRepository.Table
                          where productpricewatch.ProductId == productId && productpricewatch.IsPriceWatch == true
                          select productpricewatch.CustomerId).ToArray();
            var customers = from customer in _customerRepository.Table
                            where customerIds.Contains(customer.Id)
                            select customer;
            return customers.ToList();

        }

        public bool PriceWatchByCurrentCustomerIsExist(int productId, int customerId)
        {

            return _productPriceWatchRepository.Table.Any(x => x.ProductId == productId && x.CustomerId == customerId);
        }

        public bool InsertProductPriceWatch(BS_ProductPriceWatch productPriceWatch)
        {
            if (productPriceWatch == null)
                throw new ArgumentNullException("productPriceWatch");
                try
                {
                    _productPriceWatchRepository.Insert(productPriceWatch);
                    return true;
                }
                catch
                {
                    return false;
                }
            
        }

        public bool UpdateProductPriceWatch(BS_ProductPriceWatch productPriceWatch)
        {
            if (productPriceWatch == null)
                throw new ArgumentNullException("productPriceWatch");
            var productPricewatchRow =
                _productPriceWatchRepository.Table.FirstOrDefault(
                    p => p.ProductId == productPriceWatch.ProductId && p.CustomerId == productPriceWatch.CustomerId);
            if (productPricewatchRow != null)
            {
                productPricewatchRow.IsPriceWatch = productPriceWatch.IsPriceWatch;
                try
                {
                    _productPriceWatchRepository.Update(productPricewatchRow);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public void DeleteProductPriceWatch(BS_ProductPriceWatch productPriceWatch)
        {
            if (productPriceWatch == null)
                throw new ArgumentNullException("productPriceWatch");

            _productPriceWatchRepository.Delete(productPriceWatch);
        }
    }
}
