﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Misc.BsWebApi.Domain;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Services
{
    public class ManufacturerServiceApi : IManufacturerServiceApi
    {
        #region Fields

        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<LocalizedProperty> _localizedPropertyRepository;
        private readonly CatalogSettings _catalogSettings;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkContext _workContext;
        private readonly IRepository<BS_FeaturedProducts> _featuredProdRepository;
        private readonly IRepository<ProductSpecificationAttribute> _productSpecificationAttributeRepository;
        
        #endregion

        #region Constructor
        public ManufacturerServiceApi(IRepository<Product> productRepository,
            IRepository<LocalizedProperty> localizedPropertyRepository,
            CatalogSettings catalogSettings,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
             ILanguageService languageService,
            IWorkContext workContext,
            IRepository<BS_FeaturedProducts> featuredProdRepository,
            IRepository<ProductSpecificationAttribute> productSpecificationAttributeRepository
            )
        {
            this._productRepository = productRepository;
            this._localizedPropertyRepository = localizedPropertyRepository;
            this._catalogSettings = catalogSettings;
            this._aclRepository = aclRepository;
            this._storeMappingRepository = storeMappingRepository;
            this._languageService = languageService;
            this._workContext=workContext;
            this._featuredProdRepository = featuredProdRepository;
            this._productSpecificationAttributeRepository = productSpecificationAttributeRepository;
        }

        #endregion


        public IPagedList<Manufacturer> GetManufacturersByCategoryId(string manufacturerName = "", int pageIndex = 0, int pageSize = 2147483647, bool showHidden = false)
        {
            throw new NotImplementedException();
        }
    }
}
