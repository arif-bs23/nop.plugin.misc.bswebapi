﻿using Nop.Plugin.Misc.BsWebApi.Models._ResponseModel;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Models.SliderImageModel
{
    public class SliderBannerModel : BaseResponse
    {
        public SliderBannerModel()
        {
          
        }
            public string ImageUrl { get; set; }
            public bool IsSlider { get; set; }
            public bool IsBanner { get; set; }
        
    }
}
