﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Customer
{
    public partial class CustomerAvatarModel : BaseNopModel
    {
        public string AvatarUrl { get; set; }
    }
}