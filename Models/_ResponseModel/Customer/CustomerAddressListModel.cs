﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.Misc.BsWebApi.Models._Common;
using Nop.Web.Models.Common;


namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Customer
{
    public partial class CustomerAddressListModel : BaseNopModel
    {
        public CustomerAddressListModel()
        {
            Addresses = new List<AddressModel>();
        }

        public IList<AddressModel> Addresses { get; set; }
    }
}