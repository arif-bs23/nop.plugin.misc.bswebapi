﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;
using Nop.Web.Models.Media;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel
{
    //ErrorCode=0 no error
    //ErrorCode=1 authentication error 
    //ErrorCode=2 Error message Show From api 
    //ErrorCode=3 unknown error 
    public class GeneralResponseModel<TResult> : BaseResponse
    {
        public TResult Data { get; set; }
    }

    public class NewProductResponseModel<TResult> : BaseResponse
    {
        public TResult Products { get; set; }
        public int TotalPages { get; set; } 
    }

    public class SubscribePopupPostResponseModel<TResult> : BaseResponse
    {
        public TResult Data { get; set; }
        public int CheckErrorResponce { get; set; }
    }


    public class OnSaleProductResponseModel<TResult> : BaseResponse
    {
        public List<ProductOverViewModelApi> Products { get; set; }
        public List<SubCategoryModelApi> SubCategories { get; set; }
        public int TotalPages { get; set; }
    }

    public class OnSaleCategoryResponseModel<TResult> : BaseResponse
    {
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }
        public PictureModel PictureModel { get; set; }
        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }
        public bool DisplayCategoryBreadcrumb { get; set; }
        public IList<SubCategoryModelApi> SubCategories { get; set; }
        public IList<ProductOverViewModelApi> FeaturedProducts { get; set; }
        public IList<ProductOverViewModelApi> Products { get; set; }
        public IList<ProductTag> Tags { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProductCount { get; set; }
        public int TotalPages { get; set; }
    }

    public class SearchTermAutoCompleteResponceModel<TResult> : BaseResponse
    {
        public List<SearchTermAutoCompleteApiModel> Categories { get; set; }
       
    }

    public class IsProductPriceWatchResponseModel<TResult> : BaseResponse
    {
        
        public bool IspriceWatch { get; set; }
    }

    public class PriceWatchProductResponseModel<TResult> : BaseResponse
    {
        public TResult Products { get; set; }
        public int TotalPages { get; set; }
    }

    public class SameDiscountApplyedProductsResponseModel<TResult> : BaseResponse
    {
        public TResult Products { get; set; }
        public int TotalPages { get; set; }
        public bool BuyanyToSaveTypeDiscount { get; set; }
    }

    
}
