﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel
{
    public class LanguageSelectorResponseModel
    {
        public bool RightToLeft { get; set; }

        public string ReturenUrl { get; set; }
    }
}
