﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.BsWebApi.Models._Common;
using Nop.Web.Models.Common;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Checkout
{
    public class CheckoutBillingAddressResponseModel : ExistingAddressCommonResponseModel
    {
         public CheckoutBillingAddressResponseModel()
        {
            NewAddress = new AddressModel();
        }
        public AddressModel NewAddress { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool NewAddressPreselected { get; set; }
    }
}
