﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Checkout
{
    public class CheckoutAttributeListResponceModel : BaseResponse
    {
        public CheckoutAttributeListResponceModel()
        {
            CheckoutAttributes = new List<CheckoutAttributeResponceModel>();
        }
        public IList<CheckoutAttributeResponceModel> CheckoutAttributes { get; set; }
        public partial class CheckoutAttributeResponceModel : BaseNopEntityModel
        {
            public CheckoutAttributeResponceModel()
            {
                AllowedFileExtensions = new List<string>();
                Values = new List<CheckoutAttributeValueResponceModel>();
            }
            public string Name { get; set; }

            public string DefaultValue { get; set; }

            public string TextPrompt { get; set; }

            public bool IsRequired { get; set; }

            /// <summary>
            /// Selected day value for datepicker
            /// </summary>
            public int? SelectedDay { get; set; }
            /// <summary>
            /// Selected month value for datepicker
            /// </summary>
            public int? SelectedMonth { get; set; }
            /// <summary>
            /// Selected year value for datepicker
            /// </summary>
            public int? SelectedYear { get; set; }

            /// <summary>
            /// Allowed file extensions for customer uploaded files
            /// </summary>
            public IList<string> AllowedFileExtensions { get; set; }

            public AttributeControlType AttributeControlType { get; set; }

            public IList<CheckoutAttributeValueResponceModel> Values { get; set; }

            public partial class CheckoutAttributeValueResponceModel : BaseNopEntityModel
            {
                public string Name { get; set; }

                public string ColorSquaresRgb { get; set; }

                public string PriceAdjustment { get; set; }

                public bool IsPreSelected { get; set; }
            }
        }
    }
}
