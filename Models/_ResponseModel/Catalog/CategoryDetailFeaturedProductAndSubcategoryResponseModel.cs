﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models._Common;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Catalog
{
    public class CategoryDetailFeaturedProductAndSubcategoryResponseModel :BaseResponse
    {
        public CategoryDetailFeaturedProductAndSubcategoryResponseModel()
        {
            FeaturedProducts = new List<ProductOverViewModelApi>();
            SubCategories = new List<SubCategoryModelApi>();
        }
        public IList<SubCategoryModelApi> SubCategories { get; set; }
        public IList<ProductOverViewModelApi> FeaturedProducts { get; set; }
  
    }
}
