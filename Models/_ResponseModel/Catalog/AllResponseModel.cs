﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models._Common;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Catalog
{
    public class AllResponseModel: BaseResponse
    {
        public IList<CategoryNavigationModelApi> Data { get; set; }
        public int Count { get; set; }

        public LanguageNavSelectorModel Language { get; set; }

        public CurrencyNavSelectorModel Currency { get; set; }
    }
}
