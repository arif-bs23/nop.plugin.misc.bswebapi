﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Catalog
{
    public class SearchProductResponseModel
    {
        public SearchProductResponseModel()
        {
            Products = new List<ProductOverViewModelApi>();
            PriceRange = new List<CatalogPagingFilteringModel.PriceRangeFilterItem>();
            AvailableSortOptions = new List<SelectListItem>();
        }

        public IList<CatalogPagingFilteringModel.PriceRangeFilterItem> PriceRange { get; set; }
        public IList<ProductOverViewModelApi> Products { get; set; }
        public int TotalPages { get; set; }
        public IList<SelectListItem> AvailableSortOptions { get; set; }
    }
}
