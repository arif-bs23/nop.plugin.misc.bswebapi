﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models._Common;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Catalog
{
    public class ManufacturerDetailFeaturedProductResponseModel :BaseResponse
    {
        public ManufacturerDetailFeaturedProductResponseModel()
        {
            FeaturedProducts = new List<ProductOverViewModelApi>();
            
        }
      
        public IList<ProductOverViewModelApi> FeaturedProducts { get; set; }
  
    }
}
