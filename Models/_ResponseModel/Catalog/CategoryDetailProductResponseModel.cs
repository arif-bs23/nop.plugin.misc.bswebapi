﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Catalog
{
    public class CategoryDetailProductResponseModel : BaseResponse
    {
        public CategoryDetailProductResponseModel()
        {
            Products = new List<ProductOverViewModelApi>();
            NotFilteredItems=new List<CatalogPagingFilteringModel.SpecificationFilterItem>();
            AlreadyFilteredItems = new List<CatalogPagingFilteringModel.SpecificationFilterItem>();
            PriceRange = new List<CatalogPagingFilteringModel.PriceRangeFilterItem>();
            Brands = new List<CatalogPagingFilteringModel.BrandFilterItem>();
            Tags = new List<ProductTag>();
            AvailableSortOptions = new List<SelectListItem>();
            AvailableSearchOptions = new List<SelectListItem>();
            AvailableFilterOptions = new List<SelectListItem>();
        }

        public string Name { get; set; }
        public IList<CatalogPagingFilteringModel.PriceRangeFilterItem> PriceRange { get; set; }
        public IList<ProductOverViewModelApi> Products { get; set; }
        public IList<CatalogPagingFilteringModel.SpecificationFilterItem> NotFilteredItems { get; set; }
        public IList<CatalogPagingFilteringModel.SpecificationFilterItem> AlreadyFilteredItems { get; set; }
        public IList<SelectListItem> AvailableSortOptions { get; set; }
        public IList<SelectListItem> AvailableSearchOptions { get; set; }
        public IList<SelectListItem> AvailableFilterOptions { get; set; }

        public int TotalPages { get; set; }

        public IList<CatalogPagingFilteringModel.BrandFilterItem> Brands { get; set; }

        public IList<ProductTag> Tags { get; set; }
    }

}
