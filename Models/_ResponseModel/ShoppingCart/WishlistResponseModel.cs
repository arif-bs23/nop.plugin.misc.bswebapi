﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Plugin.Misc.BsWebApi.Models._Common;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Media;
using Nop.Web.Models.Catalog;


namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.ShoppingCart
{
    public partial class WishlistResponseModel : BaseResponse
    {
        public WishlistResponseModel()
        {
            Items = new List<ShoppingCartItemModel>();
            Warnings = new List<string>();
        }

        public Guid CustomerGuid { get; set; }
        public string CustomerFullname { get; set; }

        public bool EmailWishlistEnabled { get; set; }

        public bool ShowSku { get; set; }

        public bool ShowProductImages { get; set; }

        public bool IsEditable { get; set; }

        public bool DisplayAddToCart { get; set; }

        public bool DisplayTaxShippingInfo { get; set; }

        public IList<ShoppingCartItemModel> Items { get; set; }

        public IList<string> Warnings { get; set; }

        public int Count { get; set; }

        #region Nested Classes

        public partial class ShoppingCartItemModel : BaseNopEntityModel
        {
            public ShoppingCartItemModel()
            {
                Picture = new PictureModel();
                ProductSpecifications = new List<ProductSpecificationModel>();
                ShoppingCartWishlistItemDisCount = new ShoppingCartWishlistItemDisCountModel();
                AllowedQuantities = new List<SelectListItem>();
                Warnings = new List<string>();
            }
            public string Sku { get; set; }

            public PictureModel Picture {get;set;}

            public int ProductId { get; set; }

            public string ProductName { get; set; }

            public string ProductSeName { get; set; }

            public string UnitPrice { get; set; }

            public string SubTotal { get; set; }

            public string Discount { get; set; }

            public int Quantity { get; set; }
            public int StockQuantity { get; set; }
            public int MaximumCartQuantity { get; set; }
            public int MinimumCartQuantity { get; set; }
            public List<SelectListItem> AllowedQuantities { get; set; }
            
            public string AttributeInfo { get; set; }

            public string RecurringInfo { get; set; }

            public string RentalInfo { get; set; }

            public IList<string> Warnings { get; set; }
            public bool IsPriceWatch { get; set; }
            public bool ProductStatusNew { get; set; }
            public IList<ProductSpecificationModel> ProductSpecifications { get; set; }
            public ShoppingCartWishlistItemDisCountModel ShoppingCartWishlistItemDisCount { get; set; }
            public partial class ShoppingCartWishlistItemDisCountModel
            {
                public int Id { get; set; }
                public string Name { get; set; }
                public bool UsePercentage { get; set; }
                public decimal DiscountAmount { get; set; }
                public bool IsBundleProductDiscount { get; set; }

            }
        }

		#endregion
    }
}