﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel.Banner
{
    public class HomePageBannerResponseModel : GeneralResponseModel<IList<HomePageBannerResponseModel.BannerModel>>
    {
        public bool IsEnabled { get; set; }

        #region nested class
        public class BannerModel
        {
            public string ImageUrl { get; set; }

            public string Alt { get; set; }

            public string Title { get; set; }

            public string ShortDescription { get; set; }

            public int CampaignType { get; set; }

            public int SliderImageFor { get; set; }

            public int IsProduct { get; set; }

            public string ProdOrCatId { get; set; }
        }
        #endregion
    }
}
