﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.BsWebApi.Extensions;

namespace Nop.Plugin.Misc.BsWebApi.Models._ResponseModel
{
    public class BaseResponse
    {
        public BaseResponse()
        {
            StatusCode = (int)ErrorType.Ok;
            ErrorList=new List<string>();
        }

        public string SuccessMessage { get; set; }
        public int StatusCode { get; set; }
        public List<string> ErrorList { get; set; }
    }
}
