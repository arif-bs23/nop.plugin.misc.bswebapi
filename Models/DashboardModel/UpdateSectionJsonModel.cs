﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{

    public class UpdateSectionJsonModel
    {
        public string name { get; set; }
        public string html { get; set; }
    }
}
