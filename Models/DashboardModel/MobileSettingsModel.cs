﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public class MobileSettingsModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.ActivatePushNotification")]
        public bool ActivatePushNotification { get; set; }
        public bool ActivatePushNotification_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.SandboxMode")]
        public bool SandboxMode { get; set; }
        public bool SandboxMode_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.GcmApiKey")]
        public string GcmApiKey { get; set; }
        public bool GcmApiKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.GoogleApiProjectNumber")]
        public string GoogleApiProjectNumber { get; set; }
        public bool GoogleApiProjectNumber_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.UploudeIOSPEMFile")]
        [UIHint("Download")]
        public int UploudeIOSPEMFile { get; set; }
        public bool UploudeIOSPEMFile_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.PEMPassword")]
        public string PEMPassword { get; set; }
        public bool PEMPassword_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppNameOnGooglePlayStore")]
        public string AppNameOnGooglePlayStore { get; set; }
        public bool AppNameOnGooglePlayStore_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppUrlOnGooglePlayStore")]
        public string AppUrlOnGooglePlayStore { get; set; }
        public bool AppUrlOnGooglePlayStore_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppNameOnAppleStore")]
        public string AppNameOnAppleStore { get; set; }
        public bool AppNameOnAppleStore_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppUrlonAppleStore")]
        public string AppUrlonAppleStore { get; set; }
        public bool AppUrlonAppleStore_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppDescription")]
        public string AppDescription { get; set; }
        public bool AppDescription_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppImage")]
        [UIHint("Picture")]
        public int AppImage { get; set; }
        public bool AppImage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppLogo")]
        [UIHint("Picture")]
        public int AppLogo { get; set; }
        public bool AppLogo_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppLogoAltText")]
        public string AppLogoAltText { get; set; }
        public bool AppLogoAltText_OverrideForStore { get; set; }

    }
}