﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public partial class CustomJsonResult
    {
        public string redirect { get; set; }

        public bool success { get; set; }

        public string message { get; set; }

    }
}
