﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public class ContactInfoModel :BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [Required]
        [NopResourceDisplayName("Plugins.NopMobile.WebApi.ContactNumber")]
        public string MobContactNumber { get; set; }
        public bool MobContactNumber_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.ContactEmail")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string MobContactEmail { get; set; }
        public bool MobContactEmail_OverrideForStore { get; set; }
    }
}
