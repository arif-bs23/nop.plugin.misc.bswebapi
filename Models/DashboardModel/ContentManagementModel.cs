﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public class ContentManagementModel : BaseNopModel
    {
        public ContentManagementModel()
        {
            AvailableStores = new List<SelectListItem>();
        }
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.DefaultNopFlowSameAs")]
        public bool DefaultNopFlowSameAs { get; set; }
        public bool DefaultNopFlowSameAs_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Topics.List.SearchStore")]
        public int SearchStoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}