namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    /// <summary>
    /// Represents an error code
    /// </summary>
    public enum ErroMessageType
    {
        /// <summary>
        /// 404
        /// </summary>
        InvokeHttp404 = 404
    }
}
