﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Plugin.Misc.BsWebApi.Models.Catalog;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public class CategoryIconsModel : BaseNopModel
    {
        public CategoryIconsModel()
        {
            AvailableSubCategories = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.Willusedefaultnopcategory")]
        public bool Willusedefaultnopcategory { get; set; }
        public bool Willusedefaultnopcategory_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.SubCategory")]
        public int SubCategoryId { get; set; }
        public bool SubCategoryId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.CategoryIcon")]
        //[UIHint("Picture")]
        public string Extension { get; set; }
        public bool Extension_OverrideForStore { get; set; }

        public List<SelectListItem> AvailableSubCategories { get; set; }

    }
}