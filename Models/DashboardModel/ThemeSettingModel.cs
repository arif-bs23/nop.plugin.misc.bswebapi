﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public class ThemeSettingModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.HeaderBackgroundColor")]
        public string HeaderBackgroundColor { get; set; }
        public bool HeaderBackgroundColor_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.HeaderFontandIconColor")]
        public string HeaderFontandIconColor { get; set; }
        public bool HeaderFontandIconColor_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.HighlightedTextColor")]
        public string HighlightedTextColor { get; set; }
        public bool HighlightedTextColor_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.PrimaryTextColor")]
        public string PrimaryTextColor { get; set; }
        public bool PrimaryTextColor_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.SecondaryTextColor")]
        public string SecondaryTextColor { get; set; }
        public bool SecondaryTextColor_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.BackgroundColorofPrimaryButton")]
        public string BackgroundColorofPrimaryButton { get; set; }
        public bool BackgroundColorofPrimaryButton_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.TextColorofPrimaryButton")]
        public string TextColorofPrimaryButton { get; set; }
        public bool TextColorofPrimaryButton_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.BackgroundColorofSecondaryButton")]
        public string BackgroundColorofSecondaryButton { get; set; }
        public bool BackgroundColorofSecondaryButton_OverrideForStore { get; set; }

    }
}