﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Models.DashboardModel
{
    public class ConfigureModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppName")]
        public string AppName { get; set; }
        public bool AppName_OverrideForStore { get; set; }
     
        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AppKey")]
        public string AppKey { get; set; }
        public bool AppKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.LicenceType")]
        public string LicenceType { get; set; }
        public bool LicenceType_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.CreatedDate")]
        public string CreatedDate { get; set; }
        public bool CreatedDate_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.AndroidAppStatus")]
        public string AndroidAppStatus { get; set; }
        public bool AndroidAppStatus_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.DownloadUrl")]
        public string DownloadUrl { get; set; }
        public bool DownloadUrl_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.iOsAPPUDID")]
        public string iOsAPPUDID { get; set; }
        public bool iOsAPPUDID_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.NopMobile.WebApi.MobilWebsiteURL")]
        public string MobilWebsiteURL { get; set; }
        public bool MobilWebsiteURL_OverrideForStore { get; set; }

      
    }
}