﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Media;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class SubCategoryModelApi : IEquatable<SubCategoryModelApi>
    {
        public SubCategoryModelApi()
        {
            PictureModel = new PictureModel();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public PictureModel PictureModel { get; set; }

        public bool Equals(SubCategoryModelApi other)
        {

            //Check whether the compared object is null.
            if (Object.ReferenceEquals(other, null)) return false;

            //Check whether the compared object references the same data.
            if (Object.ReferenceEquals(this, other)) return true;

            //Check whether the SubCategoryModelApi' properties are equal.
            return Id == other.Id && Name.Equals(other.Name);
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public override int GetHashCode()
        {

            //Get hash code for the Name field if it is not null.
            int hashName = Name == null ? 0 : Name.GetHashCode();

            //Get hash code for the Code field.
            int hashId = Id.GetHashCode();

            //Calculate the hash code for the product.
            return hashName ^ hashId;
        }
    }
}
