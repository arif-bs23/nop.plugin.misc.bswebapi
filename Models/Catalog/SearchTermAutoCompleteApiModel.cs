﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
   public  class SearchTermAutoCompleteApiModel
    {
       public int Id { get; set; }
       public string CategoryName { get; set; }
    }
}
