﻿using Nop.Web.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class MenufactureOverViewModelApi
    {
         public MenufactureOverViewModelApi()
        {
            DefaultPictureModel = new PictureModel();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public PictureModel DefaultPictureModel ;
    }
}
