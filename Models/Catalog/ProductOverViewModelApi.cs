﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Misc.BsWebApi.Models._Common;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Media;
using Nop.Web.Models.Catalog;


namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class ProductOverViewModelApi : BaseNopEntityModel
    {
        public ProductOverViewModelApi()
        {
            ProductPrice = new ProductPriceModel();
            DefaultPictureModel = new PictureModel();
            ReviewOverviewModel = new ProductReviewOverviewModel();
            ProductDisCount = new ProductDisCountModel();
            ProductSpecifications = new List<ProductSpecificationModel>();
            
        }

        public string Name { get; set; }
        public string ShortDescription { get; set; }
        //status
        public bool ProductStatusNew { get; set; }
        //stock
        public bool StockAvailability { get; set; }
        public int StockQuantity { get; set; }
        public int MaximumCartQuantity { get; set; }
        public int MinimumCartQuantity { get; set; }
        //weight
        public string ProductWeight { get; set; }
        //price
        public ProductPriceModel ProductPrice { get; set; }
        //picture
        public PictureModel DefaultPictureModel { get; set; }
        //price
        public ProductReviewOverviewModel ReviewOverviewModel { get; set; }
        public IList<ProductSpecificationModel> ProductSpecifications { get; set; }

		#region Nested Classes

        public partial class ProductPriceModel 
        {
            public string OldPrice { get; set; }
            public string Price {get;set;}
        }
        
        public partial class ProductReviewOverviewModel 
        {
            public int ProductId { get; set; }
            public int RatingSum { get; set; }
            public bool AllowCustomerReviews { get; set; }
            public int TotalReviews { get; set; }
        }

        public ProductDisCountModel ProductDisCount { get; set; }
        public partial class ProductDisCountModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public bool UsePercentage { get; set; }
            public decimal DiscountAmount { get; set; }
            public bool IsBundleProductDiscount { get; set; }
            
        }
		#endregion
    }
}
