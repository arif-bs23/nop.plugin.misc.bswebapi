﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.BsWebApi.Models._ResponseModel;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class OnSaleProductModel 
    {
        public OnSaleProductModel()
        {
            Products = new List<ProductOverViewModelApi>();
            SubCategories = new List<SubCategoryModelApi>();
        }

        public List<ProductOverViewModelApi> Products { get; set; }

        public List<SubCategoryModelApi> SubCategories { get; set; }
    }
}
