﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    /// <summary>
    /// All = 0,
    /// Brands = 3,
    /// Price = 5,
    /// Specialty = 7
    /// /// </summary>
    public enum FilterByList
    {
        //All = 0,
        Brands = 3,
        Price = 5,
        Specialty = 7
    }
}
