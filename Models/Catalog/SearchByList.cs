﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    /// <summary>
    /// All = 0,
    /// New = 5,
    /// OnSale = 7,
    /// InStock = 9
    /// </summary>
    public enum SearchByList
    {
        All = 0,
        New = 5,
        OnSale = 7,
        InStock = 9
    }
}
