﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Media;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class CategoryOverViewModelApi : CategoryBaseModelApi
    {
        public CategoryOverViewModelApi()
        {
            DefaultPictureModel = new PictureModel();
        }
        public PictureModel DefaultPictureModel ;
    }
}
