﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class CategoryNavigationModelApi : CategoryBaseModelApi
    {
        public int ParentCategoryId { get; set; }
        public int DisplayOrder { get; set; }
        public string IconPath { get; set; }
        public string Extension { get; set; }
    }
}
