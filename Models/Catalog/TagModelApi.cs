﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Plugin.Misc.BsWebApi.Models._Common;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    public class TagModelApi:CategoryBaseModelApi
    {
        public TagModelApi()
        {
            Products = new List<ProductOverViewModelApi>();
            PagingFilteringContext = new CatalogPagingFilteringModel();
           
        }

        public string TagName { get; set; }
        
        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }

        public IList<ProductOverViewModelApi> Products { get; set; }
        
    }
}
