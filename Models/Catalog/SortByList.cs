﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models.Catalog
{
    /// <summary>
    /// All = 0,
    /// Savings = 1,
    /// Popularity = 2,
    /// RecentlyAdded = 3,
    /// PriceHighToLow = 4,
    /// PriceLowToHigh = 5,
    /// BrandAToZ = 6,
    /// BrandZToA = 7
    /// </summary>
    public enum SortByList
    {
       // All = 0,
       // Savings = 1,
        Position = 1,
        Popularity = 2,
        RecentlyAdded = 3,
        PriceHighToLow = 4,
        PriceLowToHigh = 5,
        BrandAToZ = 6,
        BrandZToA = 7

        //Position = 0,
        //NameAsc = 5,
        //NameDesc = 6,
        //PriceAsc = 10,
        //PriceDesc = 11,
        //CreatedOn = 15,
    }
}
