﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Models._QueryModel.Device
{
    public class AppStartModel
    {
        public int DeviceTypeId { get; set; }
        public string SubscriptionId { get; set; }

        public string EmailAddress { get; set; }
    }
}
