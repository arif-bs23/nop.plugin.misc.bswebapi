using Nop.Data.Mapping;
using Nop.Plugin.Misc.BsWebApi.Domain;

namespace Nop.Plugin.Misc.BsWebApi.Data
{
    public partial class DeviceMap : NopEntityTypeConfiguration<Device>
    {
        public DeviceMap()
        {
            this.ToTable("Bs_WebApi_Device");
            this.HasKey(x => x.Id);
            //this.Ignore(x => x.DeviceType);
        }
    }
}