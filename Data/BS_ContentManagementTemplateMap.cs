﻿using Nop.Plugin.Misc.BsWebApi.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.BsWebApi.Data
{
    public partial class BS_ContentManagementTemplateMap : EntityTypeConfiguration<BS_ContentManagementTemplate>
    {
        public BS_ContentManagementTemplateMap()
        {
            this.ToTable("BS_ContentManagementTemplate");
            this.HasKey(t => t.Id);
            this.Property(t => t.Name).IsRequired().HasMaxLength(400);
            this.Property(t => t.ViewPath).IsRequired().HasMaxLength(400);
        }
    }
}
