﻿using Nop.Plugin.Misc.BsWebApi.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.BsWebApi.Data
{
    public partial class BS_FeaturedProductsMap : EntityTypeConfiguration<BS_FeaturedProducts>
    {
        public BS_FeaturedProductsMap()
        {
            this.ToTable("BS_FeaturedProducts");
            this.HasKey(x => x.Id);
        }
    }
}
