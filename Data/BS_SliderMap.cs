﻿using Nop.Plugin.Misc.BsWebApi.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.BsWebApi.Data
{
    public partial class BS_SliderMap : EntityTypeConfiguration<BS_Slider>
    {
        public BS_SliderMap()
        {
            this.ToTable("BS_Slider");
            this.HasKey(x => x.Id);
            this.Property(x => x.ShortDescription).HasColumnType("text");
        }
    }
}
