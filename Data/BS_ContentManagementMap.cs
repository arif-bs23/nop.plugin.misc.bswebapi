﻿using Nop.Plugin.Misc.BsWebApi.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.BsWebApi.Data
{
    public partial class BS_ContentManagementMap : EntityTypeConfiguration<BS_ContentManagement>
    {
        public BS_ContentManagementMap()
        {
            this.ToTable("BS_ContentManagement");
            this.HasKey(t => t.Id);
        }
    }
}
