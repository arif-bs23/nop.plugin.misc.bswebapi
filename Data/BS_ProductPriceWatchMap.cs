﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data.Mapping;
using Nop.Plugin.Misc.BsWebApi.Domain;

namespace Nop.Plugin.Misc.BsWebApi.Data
{
    public partial class BS_ProductPriceWatchMap : EntityTypeConfiguration<BS_ProductPriceWatch>
    {
        public BS_ProductPriceWatchMap()
        {
            this.ToTable("BS_ProductPriceWatch");
            this.HasKey(x => x.Id);
        }

    }
}
