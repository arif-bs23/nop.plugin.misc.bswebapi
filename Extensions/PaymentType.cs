﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Extensions
{
    public enum PaymentType
    {
        CashOnDelivery = 1,
        PayPal = 2,
        AuthorizeDotNet=3,
        ReDirectType = 4
    }
}
