﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.BsWebApi.Extensions
{
    public static class MethodExtention
    {
        /// <summary>
        /// var start = new DateTime(2010, 1, 1);
        /// var end = new DateTime(2011, 1, 1);
        /// var query = list.Where(l => l.DateValue.IsBetween(start, end));
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool IsBetween(this DateTime dt, DateTime start, DateTime end)
        {
            return (dt >= start && dt <= end);
        }

        public static bool IsBetween(this int x, int start, int end)
        {
            return (start <= x && x <= end);
        }
    }
}
