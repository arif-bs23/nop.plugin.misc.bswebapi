﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Misc.BsWebApi.Extensions
{
    public static class HelperExtension
    {
        private const string FilePathLocation = "Plugins/Misc.BsWebApi/Content/IconPackage/";
        private const string DefaultImageName = "DefaultIcon.png";
        public static Guid GetGuid(string deviceId)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(deviceId));
                Guid result = new Guid(hash);
                return result;
            }
        }

        public static string GetEnumDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string IconImagePath(this string fileName,IWebHelper webHelper)
        {
            string imagesDirectoryPath = webHelper.MapPath("~/"+FilePathLocation);
           var filePath = Path.Combine(imagesDirectoryPath, fileName);
            string url = webHelper.GetStoreLocation()
                         + FilePathLocation;
           if (!File.Exists(filePath))
           {
                url =  url+ DefaultImageName;
               
           }
           else{
               url=url+ fileName;
           }
            return url;
        }
    }
}
