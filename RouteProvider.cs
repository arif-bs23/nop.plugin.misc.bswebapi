﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;
using System.Web.Http;

namespace Nop.Plugin.Misc.BsWebApi
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            var config = GlobalConfiguration.Configuration;

            //config.EnableCors();
            config.MessageHandlers.Add(new CorsHandler());

            routes.MapHttpRoute(
                name: "DefaultWeb",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            routes.MapRoute("BsWebApiConfigure",
                "Plugins/BsWebApi/Configure",
                new { controller = "BsWebApiConfiguration", action = "Configure" },
                new[] { "Nop.Plugin.Misc.BsWebApi.Controllers" }
           );

            routes.MapRoute("BsWebApiConfigure.SliderImageAdd", "Plugin/Misc/BsWebApiConfiguration/SliderImageAdd",
            new
            {
                controller = "BsWebApiConfiguration",
                action = "SliderImageAdd"
            },
            new[] { "Nop.Plugin.Misc.BsWebApi.Controllers" });
        }


        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
