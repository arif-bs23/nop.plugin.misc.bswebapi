using Autofac;
using Autofac.Core;
using Nop.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Misc.BsWebApi.Data;
using Nop.Plugin.Misc.BsWebApi.Domain;
using Nop.Plugin.Misc.BsWebApi.Services;
using Nop.Services.Common;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.BsWebApi.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        private const string CONTEXT_NAME = "nop_object_context_Bs_WebApi";
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder,NopConfig config)
        {

            this.RegisterPluginDataContext<BswebApiObjectContext>(builder, CONTEXT_NAME);
            builder.RegisterType<ProductServiceApi>().As<IProductServiceApi>().InstancePerLifetimeScope();
            builder.RegisterType<CustomerServiceApi>().As<ICustomerServiceApi>().InstancePerLifetimeScope();
            builder.RegisterType<DeviceService>().As<IDeviceService>().InstancePerLifetimeScope();

            //work context
            builder.RegisterType<ApiWebWorkContext>().As<IWorkContext>().InstancePerLifetimeScope();
            //data context
            builder.RegisterType<GenericAttributeServiceApi>().As<IGenericAttributeService>().InstancePerLifetimeScope();
            builder.RegisterType<EfRepository<Device>>()
                .As<IRepository<Device>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();
        }

        public int Order
        {
            get { return 4; }
        }

      

        
    }
}
