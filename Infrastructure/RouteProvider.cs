﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Plugin.Misc.BsWebApi.Infrastructure.WebApi;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;


namespace Nop.Plugin.Misc.BsWebApi.Infrastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapLocalizedRoute("Plugin.Misc.BswebAPi",
                "KeepAlive/indexApi",
                new { controller = "KeepAliveApi", action = "Index", area = "" },
                new[] { "Nop.Plugin.Misc.BsWebApi.Controllers" }
           );
            routes.MapRoute("Plugin.Misc.BswebAPi.OpcCompleteRedirectionPayment",
                    "api/checkout/OpcCompleteRedirectionPayment",
                    new { controller = "RedirectPage", action = "OpcCompleteRedirectionPayment", area = "" },
                    new[] { "Nop.Plugin.Misc.BsWebApi.Controllers" }
               );

            //web api 2
            var config = GlobalConfiguration.Configuration;
            WebApiConfig.Register(config);
           
        }
        public int Priority
        {
            get
            {
                return 2;
            }
        }
    }
}
